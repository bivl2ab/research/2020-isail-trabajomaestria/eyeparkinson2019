%% SVM: RIGHT EYE 
clear all
load DataEval_data3_DER.mat, de_data3 = DataEval;
load DataEval_results3_DER.mat, de_results3 = DataEval;
clear DataEval

layer = 1;
slices = 3;
gtlabels = [zeros(65,1);ones(65,1)]; 
disp('SVM RIGHT EYE ########################')
for net = [1 2 4 7 5]
    fprintf('net %d ===============================\n',net)
%     [~,~,AUCd(net),scrsd] = getROC(de_data3,net,layer,slices,gtlabels);
%     [~,~,AUCr(net),scrsr] = getROC(de_results3,net,layer,slices,gtlabels);
%     comp = AUC_compare_correlated([scrsd(:,2),scrsr(:,2)],gtlabels);
%     fprintf('std AUC %%95CI: %0.3f [%0.3f - %0.3f]\n',comp{1}(1),comp{1}(2),comp{1}(3))
%     fprintf('mag AUC %%95CI: %0.3f [%0.3f - %0.3f]\n',comp{2}(1),comp{2}(2),comp{2}(3))
%     fprintf('p-value: %0.3f\n',comp{4})
    [macc,std] = getAcc(de_data3,net,layer,slices);
    fprintf('std acc: %0.1f +- %0.1f\n',macc,std)
    [macc,std] = getAcc(de_results3,net,layer,slices);
    fprintf('mag acc: %0.1f +- %0.1f\n',macc,std)
end

for ly = 1:3
    fprintf('ly %d std acc = %0.1f\n',ly,getAcc(de_data3,2,ly,3)) 
    fprintf('ly %d mag acc = %0.1f\n',ly,getAcc(de_results3,2,ly,3))    
end

slc = [1 2 4 6 8];
for s = 1:3 % 1:5
    fprintf('slc %d std acc = %0.1f\n',slc(s),getAcc(de_data3,2,1,s)) 
    fprintf('slc %d mag acc = %0.1f\n',slc(s),getAcc(de_results3,2,1,s))    
end

%% SVM: LEFT EYE
clear all, clc
load DataEval_data3_IZQ.mat, de_data3 = DataEval;
load DataEval_results3_IZQ.mat, de_results3 = DataEval;
clear DataEval

layer = 1;
slices = 3;
gtlabels = [zeros(65,1);ones(65,1)]; 
disp('SVM LEFT EYE #########################')
for net = [1 2 4 7 5]
     fprintf('net %d ===============================\n',net)
%     [~,~,AUCd(net),scrsd] = getROC(de_data3,net,layer,slices,gtlabels);
%     [~,~,AUCr(net),scrsr] = getROC(de_results3,net,layer,slices,gtlabels);
%     comp = AUC_compare_correlated([scrsd(:,2),scrsr(:,2)],gtlabels);
%     fprintf('std AUC %%95CI: %0.3f [%0.3f - %0.3f]\n',comp{1}(1),comp{1}(2),comp{1}(3))
%     fprintf('mag AUC %%95CI: %0.3f [%0.3f - %0.3f]\n',comp{2}(1),comp{2}(2),comp{2}(3))
%     fprintf('p-value: %0.3f\n',comp{4})
    [macc,std] = getAcc(de_data3,net,layer,slices);
    fprintf('std acc: %0.1f +- %0.1f\n',macc,std)
    [macc,std] = getAcc(de_results3,net,layer,slices);
    fprintf('mag acc: %0.1f +- %0.1f\n',macc,std)    
end

for ly = 1:3
    fprintf('ly %d std acc = %0.1f\n',ly,getAcc(de_data3,2,ly,3)) 
    fprintf('ly %d mag acc = %0.1f\n',ly,getAcc(de_results3,2,ly,3))    
end

slc = [1 2 4 6 8];
for s = 1:3 % 1:5
    fprintf('slc %d std acc = %0.1f\n',slc(s),getAcc(de_data3,2,1,s)) 
    fprintf('slc %d mag acc = %0.1f\n',slc(s),getAcc(de_results3,2,1,s))    
end


%% SVM: STEREO
clear all, clc
load DataEval_data3_STR.mat, de_data3 = DataEval;
load DataEval_results3_STR.mat, de_results3 = DataEval;
clear DataEval

layer = 1;
slices = 3;
gtlabels = [zeros(65,1);ones(65,1)]; 
disp('SVM STEREO #########################')
for net = [1 2 4 7 5]
    fprintf('net %d ===============================\n',net)
%     [~,~,AUCd(net),scrsd] = getROC(de_data3,net,layer,slices,gtlabels);
%     [~,~,AUCr(net),scrsr] = getROC(de_results3,net,layer,slices,gtlabels);
%     comp = AUC_compare_correlated([scrsd(:,2),scrsr(:,2)],gtlabels);
%     fprintf('std AUC %%95CI: %0.3f [%0.3f - %0.3f]\n',comp{1}(1),comp{1}(2),comp{1}(3))
%     fprintf('mag AUC %%95CI: %0.3f [%0.3f - %0.3f]\n',comp{2}(1),comp{2}(2),comp{2}(3))
%     fprintf('p-value: %0.3f\n',comp{4})
    [macc,std] = getAcc(de_data3,net,layer,slices);
    fprintf('std acc: %0.1f +- %0.1f\n',macc,std)
    [macc,std] = getAcc(de_results3,net,layer,slices);
    fprintf('mag acc: %0.1f +- %0.1f\n',macc,std)    
end

for ly = 1:3
    fprintf('ly %d std acc = %0.1f\n',ly,getAcc(de_data3,2,ly,3)) 
    fprintf('ly %d mag acc = %0.1f\n',ly,getAcc(de_results3,2,ly,3))    
end

slc = [1 2 4 6 8];
for s = 3:3 % 1:5
    fprintf('slc %d std acc = %0.1f\n',slc(s),getAcc(de_data3,2,1,s)) 
    fprintf('slc %d mag acc = %0.1f\n',slc(s),getAcc(de_results3,2,1,s))    
end


%% CNN: RIGHT EYE 
clear all
load data3_DER_CNNTrain.mat acc scores
accDder = acc; scoresDder = scores;
load results3_DER_CNNTrain.mat acc scores
accRder = acc; scoresRder = scores;

catDscores = cat(1,scoresDder{:});
catRscores = cat(1,scoresRder{:});
gtlabels = [zeros(26*15/2,1);ones(26*15/2,1)];
comp = AUC_compare_correlated([catDscores(:,2),catRscores(:,2)], gtlabels);
disp('CNN RIGHT EYE ########################')
fprintf('std AUC %%95CI: %0.3f [%0.3f - %0.3f]\n',comp{1}(1),comp{1}(2),comp{1}(3))
fprintf('mag AUC %%95CI: %0.3f [%0.3f - %0.3f]\n',comp{2}(1),comp{2}(2),comp{2}(3))
fprintf('p-value: %0.3f\n',comp{4})
fprintf('std Acc: %2.1f\n',100*mean(accDder))
fprintf('mag Acc: %2.1f\n',100*mean(accRder))

%% CNN: LEFT EYE 
clear all
load data3_IZQ_CNNTrain.mat acc scores
accDizq = acc; scoresDizq = scores;
load results3_IZQ_CNNTrain.mat acc scores
accRizq = acc; scoresRizq = scores;

catDscores = cat(1,scoresDizq{:});
catRscores = cat(1,scoresRizq{:});
gtlabels = [zeros(26*15/2,1);ones(26*15/2,1)];
comp = AUC_compare_correlated([catDscores(:,2),catRscores(:,2)], gtlabels);
disp('CNN LEFT EYE ########################')
fprintf('std AUC %%95CI: %0.3f [%0.3f - %0.3f]\n',comp{1}(1),comp{1}(2),comp{1}(3))
fprintf('mag AUC %%95CI: %0.3f [%0.3f - %0.3f]\n',comp{2}(1),comp{2}(2),comp{2}(3))
fprintf('p-value: %0.3f\n',comp{4})
fprintf('std Acc: %2.1f\n',100*mean(accDizq))
fprintf('mag Acc: %2.1f\n',100*mean(accRizq))

%% CNN: STEREO 
clear all
load data3_STR_CNNTrain.mat acc scores
accDstr = acc; scoresDstr = scores;
load results3_STR_CNNTrain.mat acc scores
accRstr = acc; scoresRstr = scores;

catDscores = cat(1,scoresDstr{:});
catRscores = cat(1,scoresRstr{:});
gtlabels = [zeros(26*15/2,1);ones(26*15/2,1)];
comp = AUC_compare_correlated([catDscores(:,2),catRscores(:,2)], gtlabels);
disp('CNN LEFT EYE ########################')
fprintf('std AUC %%95CI: %0.3f [%0.3f - %0.3f]\n',comp{1}(1),comp{1}(2),comp{1}(3))
fprintf('mag AUC %%95CI: %0.3f [%0.3f - %0.3f]\n',comp{2}(1),comp{2}(2),comp{2}(3))
fprintf('p-value: %0.3f\n',comp{4})
fprintf('std Acc: %2.1f\n',100*mean(accDstr))
fprintf('mag Acc: %2.1f\n',100*mean(accRstr))

%% Pertuz Table STR %======================================================
clear all
load DataEval_data3_IZQ.mat, de_data3 = DataEval;
load DataEval_results3_IZQ.mat, de_results3 = DataEval;
clear DataEval
load data3_IZQ_CNNTrain.mat scores, kscores_dCNN = scores;
load results3_IZQ_CNNTrain.mat scores, kscores_rCNN = scores;
clear scores

n = [1 2 4 7 5];
Tscores_d = zeros(26,6); Tscores_r = zeros(26,6);
for i = 1:6
   if i <= 5 
       kscores_d = de_data3{n(i),1,3}.kscores;
       kscores_r = de_results3{n(i),1,3}.kscores;
   else
       kscores_d = kscores_dCNN;
       kscores_r = kscores_rCNN;       
   end    
   for k = 1:26
       mkscores_d(k) = mean(kscores_d{k}(:,2)); 
       mkscores_r(k) = mean(kscores_r{k}(:,2)); 
   end
   Tscores_d(:,i) =  mkscores_d';
   Tscores_r(:,i) =  mkscores_r';  
end
idx = [1 18 2 26 3 16 4 19 5 25 6 24 7 21 ...
       8 14 9 23 10 15 11 20 12 17 13 22];
Ascores_d = Tscores_d(idx,:); 
Ascores_r = Tscores_r(idx,:);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all, clc

R_std = readtable('Table_R_std.xlsx');
R_mag = readtable('Table_R_mag.xlsx');
L_std = readtable('Table_L_std.xlsx');
L_mag = readtable('Table_L_mag.xlsx');
RL_std = readtable('Table_RL_std.xlsx');
RL_mag = readtable('Table_RL_mag.xlsx');
tab_names = {'R_std','R_mag',...
             'L_std','L_mag',...
             'RL_std','RL_mag'};
printORs


%% End
