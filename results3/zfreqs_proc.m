%%
clear all, clc
addpath('zfreqs\')

for f = 10 % [1 2 3 4 5 7 8 9 10]
    disp(f)
    cd zfreqs\
    listing = dir(sprintf('*fm_%d.mat',f));
    cd ..
    DataDER = cell(7,3,3);
    for n = [1 2 4 5 7] % 1:7
        for ly = 1:1 
            for c = 3:3 
                fprintf('n%d ly%d c%d =========\n',n,ly,c)
                clear Data
                for l = 1:length(listing)
                    load(listing(l).name,'concats')
                    Data(l,:) = concats{n,ly,c}';
                end
                DataDER{n,ly,c} = Data;
            end
        end
    end
    save(sprintf('DataDER_fm_%d.mat',f),'DataDER')
end
%%
idx = cell(26,1); c = 1;
for p = 1:26
    for s = 1:5
        idx{p}(s) = c;
        c = c+1;
    end
end
labels = [zeros(65,1);ones(65,1)]; clear p s c

for f = 10 % [1 2 3 4 5 7 8 9 10]
    disp(f)
    load(sprintf('DataDER_fm_%d.mat',f))
    DataEval = cell(7,3,3);
    for n = [1 2 4 5 7] % #nets 1:7
        for ly = 1:1 % #layers 1:3
            for c = 3:3 % #concatenations 1:3 => S1, S2-U-S4, ALL
                fprintf('net%d - ly%d - c%d\n',n,ly,c)
                klabels = cell(26,1); kscores = cell(26,1);
                parfor k = 1:26
                    fprintf('fold %d\n',k)
                    Test = DataDER{n,ly,c}(idx{k},:);
                    Train = DataDER{n,ly,c}(setdiff(1:130,idx{k}),:);
                    TestLabels = labels(idx{k}); 
                    TrainLabels = labels(setdiff(1:130,idx{k}));
                    rng default
                    CVmodel = fitcsvm(Train,TrainLabels,'KernelFunction','RBF',...
                              'OptimizeHyperparameters',{'BoxConstraint','KernelScale','Standardize'},...
                              'HyperparameterOptimizationOptions',...
                                    struct('AcquisitionFunctionName','expected-improvement-plus',...
                                    'UseParallel',false,'Repartition',true,'kfold',5,'ShowPlots',false,'Verbose',0));        
                    PPCVmodel = fitSVMPosterior(CVmodel);
                    [klabels{k},kscores{k}] = predict(PPCVmodel,Test);  
                end 
                FoldsEval.klabels = klabels;
                FoldsEval.kscores = kscores;
                DataEval{n,ly,c} = FoldsEval;
                pause(1)
                getAcc(DataEval,n,ly,c)
            end
        end
    end
    save(sprintf('DataDER_fm_%d.mat',f),'DataDER','DataEval')
end
%% 
acc = zeros(15,3); 
acc(6,1) = 92.3; % R
acc(6,2) = 93.1; % L 
acc(6,3) = 95.4; % R-L
Mnk = nchoosek([1 2 4 5 7],3);
for C = 6
    for f = [1 2 3 4 5 7 8 9 10]
        load(sprintf('DataDER_fm_%d.mat',f),'DataEval')
        acc(f,1) = getAcc(DataEval,Mnk(C,1),1,3);
        acc(f,2) = getAcc(DataEval,Mnk(C,2),1,3);
        acc(f,3) = getAcc(DataEval,Mnk(C,3),1,3);
    end
    plot(acc(1:10,1),'r','LineWidth',1.2,'Marker','o','MarkerFaceColor','r','MarkerSize',4)
    hold on, grid on, ylim([75 100]), xlim([1 15])
    plot(acc(1:10,2),'b','LineWidth',1.2,'Marker','o','MarkerFaceColor','b','MarkerSize',4)
    plot(acc(1:10,3),'k','LineWidth',1.2,'Marker','o','MarkerFaceColor','k','MarkerSize',4)
    hold off
    disp(C)
    pause()
end
%% Frequency plots
clear all, clc
load('acc_freq2.mat')
% acc(:,1) => R
% acc(:,2) => L
% acc(:,3) => R-L

figure
lw = 1;
mksz = 3;
ts = 3.3;

subplot(4,2,2)
plot(acc(:,1),'r','LineWidth',lw,'Marker','o','MarkerFaceColor','r','MarkerSize',mksz)
hold on, ylim([72.5 97.5]), xlim([1 15]), xticks(1:15)
yline(81.5,'b','LineWidth',lw); xlabel('$f$ [Hz]','interpreter','latex')
ylabel('Accuracy (\%)','interpreter','latex')
text(-ts,85,'\textbf{(a) R}','interpreter','latex')
set(gca,'TickLabelInterpreter','latex','FontSize',10)
shade([4 5 6 7],100*ones(1,4),'FillColor',[147 196 227]/255,'FillAlpha',0.3)
hold off

subplot(4,2,4)
plot(acc(:,2),'r','LineWidth',lw,'Marker','o','MarkerFaceColor','r','MarkerSize',mksz)
hold on, ylim([72.5 97.5]), xlim([1 15]), xticks(1:15)
yline(78.5,'b','LineWidth',lw); xlabel('$f$ [Hz]','interpreter','latex')
ylabel('Accuracy (\%)','interpreter','latex')
text(-ts,85,'\textbf{(b) L}','interpreter','latex')
set(gca,'TickLabelInterpreter','latex','FontSize',10)
shade([4 5 6 7],100*ones(1,4),'FillColor',[147 196 227]/255,'FillAlpha',0.3)
hold off

subplot(4,2,6)
yline(80.8,'b','LineWidth',lw); xlabel('$f$ [Hz]','interpreter','latex')
hold on
plot(acc(:,3),'r','LineWidth',lw,'Marker','o','MarkerFaceColor','r','MarkerSize',mksz)
ylim([72.5 97.5]), xlim([1 15]), xticks(1:15)
ylabel('Accuracy (\%)','interpreter','latex')
set(gca,'TickLabelInterpreter','latex','FontSize',10)
shade([4 5 6 7],100*ones(1,4),'FillColor',[147 196 227]/255,'FillAlpha',0.3)
text(-ts,85,'\textbf{(c) R-L}','interpreter','latex')
box on
hold off

hold on
plot((0.5*acc(:,1)+0.5*acc(:,2)+3),'m','LineWidth',lw,'Marker','o','MarkerFaceColor','r','MarkerSize',mksz)


legend('Standard','Magnified','interpreter','latex')

savePDFfig('freqMag')


%% Ablation Plots
columns = {'Rstd','Rmag','Lstd','Lmag','RLstd','RLmag'};
rows = {'1S','2S','3S','4S','5S'};
T = array2table(zeros(5,6),'VariableNames',columns,'RowNames',rows);

clear all, clc
load Tslicesvar.mat
yax = [60 100];
xtcklbl = {'1 Slice','2 Slices','4 Slices','6 Slices','8 Slices'};
ytck = 60:10:100;
lw = 1;
mksz = 3;
gcafz = 10;

figure

subplot(4,2,2)
hold off
plot(T.Rstd,'b','LineWidth',lw,'Marker','o','MarkerFaceColor','b','MarkerSize',mksz) 
hold on
plot(T.Rmag,'r','LineWidth',lw,'Marker','o','MarkerFaceColor','r','MarkerSize',mksz)
ylim(yax), yticks(ytck), ylabel('Accuracy (\%)','interpreter','latex')
xlim([0.5 5.5]), xticks(1:5), xticklabels(xtcklbl)
set(gca,'TickLabelInterpreter','latex','FontSize',gcafz)
text(-1.3,80,'\textbf{(a) R}','interpreter','latex')

subplot(4,2,4)
hold off
plot(T.Lstd,'b','LineWidth',lw,'Marker','o','MarkerFaceColor','b','MarkerSize',mksz)
hold on
plot(T.Lmag,'r','LineWidth',lw,'Marker','o','MarkerFaceColor','r','MarkerSize',mksz)
ylim(yax), yticks(ytck), ylabel('Accuracy (\%)','interpreter','latex')
xlim([0.5 5.5]), xticks(1:5), xticklabels(xtcklbl)
set(gca,'TickLabelInterpreter','latex','FontSize',gcafz)
text(-1.3,80,'\textbf{(b) L}','interpreter','latex')

subplot(4,2,6)
hold off
plot(T.RLstd,'b','LineWidth',lw,'Marker','o','MarkerFaceColor','b','MarkerSize',mksz)
hold on
plot(T.RLmag,'r','LineWidth',lw,'Marker','o','MarkerFaceColor','r','MarkerSize',mksz)
ylim(yax), yticks(ytck), ylabel('Accuracy (\%)','interpreter','latex')
xlim([0.5 5.5]), xticks(1:5), xticklabels(xtcklbl)
set(gca,'TickLabelInterpreter','latex','FontSize',gcafz)
text(-1.3,80,'\textbf{(c) R-L}','interpreter','latex')

legend('Standard','Magnified','interpreter','latex')

savePDFfig('slicesvar')




