%% Kfold Trainings
clear all, clc
load myCNN3_4slc_stereo.mat

drloads = {'data3_DER_concatslcs.mat','data3_IZQ_concatslcs.mat',...
        'results3_DER_concatslcs.mat','results3_IZQ_concatslcs.mat'};

options = trainingOptions('adam', ...
            'InitialLearnRate',0.01, ...
            'MaxEpochs',8, ...
            'Shuffle','every-epoch', ...
            'Verbose',false, ...
            'Plots','none',... %'training-progress','none'    
            'ExecutionEnvironment','gpu');
%% Preparing
%load(drloads{1}), concatslicesD = concatslices; clear concatslices
%load(drloads{2}), concatslicesI = concatslices; clear concatslices
load(drloads{3}), concatslicesD = concatslices; clear concatslices
load(drloads{4}), concatslicesI = concatslices; clear concatslices

pslices = cell(26,5,4,4); 
for p = 1:26
    for smpl = 1:5
        for cch = 1:4
            for f = 1:4
                D = concatslicesD{smpl+(p-1)*5,cch,f};
                I = concatslicesI{smpl+(p-1)*5,cch,f};
                pslices{p,smpl,cch,f}(:,:,1:4) = D; 
                pslices{p,smpl,cch,f}(:,:,5:8) = I;                  
            end
        end
    end
end
clear concatslicesD concatslicesI D I p smpl cch f

idx = cell(26,1); c = 1;
for p = 1:26
    for s = 1:80 % 5*4*4
        idx{p}(s) = c;
        c = c+1;
    end
end
labels = [zeros(26*80/2,1);ones(26*80/2,1)]; clear p s c

acc = zeros(26,1);
kTrainedNets = cell(26,1); YPred = cell(26,1); scores = cell(26,1);
%% RUN !!
k = 7; %<##############
for k = 1:26
    disp(['Fold ',num2str(k)])
    cellTest = squeeze(pslices(k,:,1:3,1)); 
    cellTest = cellTest(:);
    imdsTest = cat(4,cellTest{:});
    labelsTest = labels(idx{k}(1:15)); 
    cellTrain = reshape(pslices(setdiff(1:26,k),:,:,:),25,[]); 
    cellTrain = cellTrain'; cellTrain = cellTrain(:);
    imdsTrain = cat(4,cellTrain{:});
    labelsTrain = labels(setdiff(1:2080,idx{k}));    
    for i = 1:10 %<##############
        disp(i) % <########
        %rng default
        kTrainedNets{i} = trainNetwork(imdsTrain,categorical(labelsTrain),layers,options);   
        [YPred{i},scores{i}] = classify(kTrainedNets{i},imdsTest);
        accTune(i) = sum(YPred{i} == categorical(labelsTest))/numel(labelsTest);
        disp(['Acc: ',num2str(accTune(i))])
        pause(3) %<##############
    end %<##############   

    clear imdsTrain cellTrain labelsTrain ...
          imdsTest cellTest labelsTest
    pause(3)
end
%save('data3_STR_CNNTrain.mat','kTrainedNets','acc','YPred','scores')
%save('results3_STR_CNNTrain.mat','kTrainedNets','acc','YPred','scores')

k9TrainedNet = kTrainedNets{8};
acck9 = accTune(8);
YPredk9 = YPred{8};
scoresk9 = scores{8};
clear kTrainedNets accTune YPred scores
save('r3_STR_k9.mat','k9TrainedNet','acck9','YPredk9','scoresk9')
clear k9TrainedNet acck9 YPredk9 scoresk9


%%