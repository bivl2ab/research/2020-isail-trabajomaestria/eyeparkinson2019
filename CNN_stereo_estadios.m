%% Kfold Trainings
clear all, clc
load myCNN3_4slc_stereo_estadios.mat

drloads = {'data3_DER_concatslcs.mat','data3_IZQ_concatslcs.mat',...
        'results3_DER_concatslcs.mat','results3_IZQ_concatslcs.mat'};

options = trainingOptions('adam', ...
            'InitialLearnRate',0.01, ...
            'MaxEpochs',10, ... 
            'Shuffle','every-epoch', ...
            'Verbose',false, ...
            'Plots','none',... %'training-progress','none'    
            'ExecutionEnvironment','gpu');
%% Preparing
%load(drloads{1}), concatslicesD = concatslices; clear concatslices
%load(drloads{2}), concatslicesI = concatslices; clear concatslices
load(drloads{3}), concatslicesD = concatslices; clear concatslices
load(drloads{4}), concatslicesI = concatslices; clear concatslices

pslices = cell(15,5,4,4); 
p = [1:5 16:25];
for ip = 1:15
    for smpl = 1:5
        for cch = 1:4
            for f = 1:4
                D = concatslicesD{smpl+(p(ip)-1)*5,cch,f};
                I = concatslicesI{smpl+(p(ip)-1)*5,cch,f};
                pslices{ip,smpl,cch,f}(:,:,1:4) = D; 
                pslices{ip,smpl,cch,f}(:,:,5:8) = I;                  
            end
        end
    end
end
clear concatslicesD concatslicesI D I p smpl cch f

idx = cell(15,1); c = 1;
for p = 1:15
    for s = 1:80 % 5*4*4
        idx{p}(s) = c;
        c = c+1;
    end
end
clear p s c
labels = [zeros(5*80,1); ...
          2*ones(1*80,1); 1*ones(3*80,1); ...
          2*ones(2*80,1); 1*ones(2*80,1); 2*ones(2*80,1)]; 
acc = zeros(15,1);
kTrainedNets = cell(15,1); YPred = cell(15,1); scores = cell(15,1);
k = 11; %<=
%% RUN !!
for k = 1:15
    disp(['Fold ',num2str(k)])
    cellTest = squeeze(pslices(k,:,1:3,1)); 
    cellTest = cellTest(:);
    imdsTest = cat(4,cellTest{:});
    labelsTest = labels(idx{k}(1:15)); 
    cellTrain = reshape(pslices(setdiff(1:15,k),:,:,:),14,[]); 
    cellTrain = cellTrain'; cellTrain = cellTrain(:);
    imdsTrain = cat(4,cellTrain{:});
    labelsTrain = labels(setdiff(1:1200,idx{k}));    
    %{
    options = trainingOptions('adam', ...
            'InitialLearnRate',0.01, ...
            'MaxEpochs',15, ... 
            'Shuffle','every-epoch', ...
            'Verbose',false, ...
            'Plots','training-progress',... %'training-progress','none'    
            'ExecutionEnvironment','gpu',...
            'ValidationData',{imdsTest,categorical(labelsTest,[0 1 2])},...
            'ValidationFrequency',10);
    %}    
    for i = 1:10
        disp(i)
        %rng default        
        kTrainedNets{i} = trainNetwork(imdsTrain,categorical(labelsTrain),layers,options);   
        [YPred{i},scores{i}] = classify(kTrainedNets{i},imdsTest);
        accTune(i) = sum(YPred{i} == categorical(labelsTest))/numel(labelsTest);
        disp(['Acc: ',num2str(accTune(i))])
        pause(3)
    end
    clear imdsTrain cellTrain labelsTrain ...
          imdsTest cellTest labelsTest
    pause(3)
end

%save('data3_STR_CNNTrain_estadios.mat','kTrainedNets','acc','YPred','scores')
save('results3_STR_CNNTrain_estadios.mat','kTrainedNets','acc','YPred','scores')




%%