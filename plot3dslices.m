ysz = 200;

I = imread('S1.png'); 
[X,Y] = meshgrid(1:180,1:ysz);
Z = 60*ones(ysz,180);
figure
warp(X,Y,Z,I,255)
zlim([1 120]), ylim([1 ysz]), xlim([1 180])
hold on

I = flip(imread('S3.png')); 
It = permute(I,[2 1 3]);
[Z,Y] = meshgrid(1:120,1:ysz);
X = 90*ones(ysz,120);
warp(X,Y,Z,It,255)

I = flip(imread('S4.png')); 
It = permute(I,[2 1 3]);
[X,Y] = meshgrid(1:180,1:ysz);
Z = (119/179)*X + (60/179);
warp(X,Y,Z,It,255)

I = imread('S2.png'); 
It = permute(I,[2 1 3]);
[X,Y] = meshgrid(1:180,1:ysz);
Z = -(119/179)*X + (21599/179);
warp(X,Y,Z,It,255)

box on
set(gca,'XTick',[])
set(gca,'YTick',[])
set(gca,'ZTick',[])


