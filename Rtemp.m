addpath('data3\IZQ\') ,cd data3\IZQ\
listing = dir('*.mat'); cd ..\..
disp('data3 IZQ =========================================================')
DataEval = cell(6,3,3);
for n = 3:6 % #nets
    for ly = 1:3 % #layers 1:3
        for c = 1:3 % #concatenations: S1, S2-U-S4, ALL
            disp(sprintf('net%d - ly%d - c%d',n,ly,c))
            clear Data
            for l = 1:length(listing)
                %disp(l)
                load(listing(l).name)
                Data(l,:) = concats{n,ly,c}'; 
            end
            klabels = cell(26,1); kscores = cell(26,1);
            parfor k = 1:26
                disp(sprintf('fold %d',k))
                Test = Data(idx{k},:);
                Train = Data(setdiff(1:130,idx{k}),:);
                TestLabels = labels(idx{k}); 
                TrainLabels = labels(setdiff(1:130,idx{k}));
                rng default
                CVmodel = fitcsvm(Train,TrainLabels,'KernelFunction','RBF',...
                          'OptimizeHyperparameters',{'BoxConstraint','KernelScale','Standardize'},...
                          'HyperparameterOptimizationOptions',...
                                struct('AcquisitionFunctionName','expected-improvement-plus',...
                                'UseParallel',false,'Repartition',true,'kfold',5,'ShowPlots',false,'Verbose',0));        
                PPCVmodel = fitSVMPosterior(CVmodel);
                [klabels{k},kscores{k}] = predict(PPCVmodel,Test);  
            end 
            FoldsEval.klabels = klabels;
            FoldsEval.kscores = kscores;
            DataEval{n,ly,c} = FoldsEval;
            pause(0.5)
        end
    end
end
save('DataEval_data3_IZQ_net3-6.mat','DataEval')
clearvars -except idx labels
%==========================================================================
rmpath('data3\IZQ\')
addpath('results3\DER\') ,cd results3\DER\
listing = dir('*.mat'); cd ..\..
disp('results3 DER ======================================================')
DataEval = cell(6,3,3);
for n = 1:6 % #nets
    for ly = 1:3 % #layers 1:3
        for c = 1:3 % #concatenations: S1, S2-U-S4, ALL
            disp(sprintf('net%d - ly%d - c%d',n,ly,c))
            clear Data
            for l = 1:length(listing)
                %disp(l)
                load(listing(l).name)
                Data(l,:) = concats{n,ly,c}'; 
            end
            klabels = cell(26,1); kscores = cell(26,1);
            parfor k = 1:26
                disp(sprintf('fold %d',k))
                Test = Data(idx{k},:);
                Train = Data(setdiff(1:130,idx{k}),:);
                TestLabels = labels(idx{k}); 
                TrainLabels = labels(setdiff(1:130,idx{k}));
                rng default
                CVmodel = fitcsvm(Train,TrainLabels,'KernelFunction','RBF',...
                          'OptimizeHyperparameters',{'BoxConstraint','KernelScale','Standardize'},...
                          'HyperparameterOptimizationOptions',...
                                struct('AcquisitionFunctionName','expected-improvement-plus',...
                                'UseParallel',false,'Repartition',true,'kfold',5,'ShowPlots',false,'Verbose',0));        
                PPCVmodel = fitSVMPosterior(CVmodel);
                [klabels{k},kscores{k}] = predict(PPCVmodel,Test);  
            end 
            FoldsEval.klabels = klabels;
            FoldsEval.kscores = kscores;
            DataEval{n,ly,c} = FoldsEval;
            pause(0.5)
        end
    end
end
save('DataEval_results3_DER.mat','DataEval')
clearvars -except idx labels
%==========================================================================
rmpath('results3\DER\')
addpath('results3\IZQ\') ,cd results3\IZQ\
listing = dir('*.mat'); cd ..\..
disp('results3 IZQ ======================================================')
DataEval = cell(6,3,3);
for n = 1:6 % #nets
    for ly = 1:3 % #layers 1:3
        for c = 1:3 % #concatenations: S1, S2-U-S4, ALL
            disp(sprintf('net%d - ly%d - c%d',n,ly,c))
            clear Data
            for l = 1:length(listing)
                %disp(l)
                load(listing(l).name)
                Data(l,:) = concats{n,ly,c}'; 
            end
            klabels = cell(26,1); kscores = cell(26,1);
            parfor k = 1:26
                disp(sprintf('fold %d',k))
                Test = Data(idx{k},:);
                Train = Data(setdiff(1:130,idx{k}),:);
                TestLabels = labels(idx{k}); 
                TrainLabels = labels(setdiff(1:130,idx{k}));
                rng default
                CVmodel = fitcsvm(Train,TrainLabels,'KernelFunction','RBF',...
                          'OptimizeHyperparameters',{'BoxConstraint','KernelScale','Standardize'},...
                          'HyperparameterOptimizationOptions',...
                                struct('AcquisitionFunctionName','expected-improvement-plus',...
                                'UseParallel',false,'Repartition',true,'kfold',5,'ShowPlots',false,'Verbose',0));        
                PPCVmodel = fitSVMPosterior(CVmodel);
                [klabels{k},kscores{k}] = predict(PPCVmodel,Test);  
            end 
            FoldsEval.klabels = klabels;
            FoldsEval.kscores = kscores;
            DataEval{n,ly,c} = FoldsEval;
            pause(0.5)
        end
    end
end
save('DataEval_results3_IZQ.mat','DataEval')
clearvars -except idx labels



