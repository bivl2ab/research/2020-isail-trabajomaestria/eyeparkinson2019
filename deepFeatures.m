%% Filenames
clear all, clc
%addpath('data3\DER\'), cd data3\DER\
%addpath('data3\IZQ\') ,cd data3\IZQ\
addpath('results3\DER\'), cd results3\DER\
%addpath('results3\IZQ\') ,cd results3\IZQ\
listing = dir('*_mag.mat'); cd ..\..

net1 = vgg19;             %sz1
net2 = resnet101;         %sz1
%net3 = inceptionv3;       %sz2
net4 = inceptionresnetv2; %sz2
net5 = densenet201;       %sz1
%net6 = mobilenetv2;       %sz1
net7 = xception;          %sz2
sz1 = [224,224]; sz2 = [299,299];

%% Computing Deep Features & Covariances
tic
Fl = cell(130,8,7,3); % (#slices,#nets,#layers)
for l = 1:1 %length(listing)
    disp(l)
    load(listing(l).name)  
    for s = 1:4
        slc_sz1 = imresize(slices_e{s},sz1);
        slc_sz2 = imresize(slices_e{s},sz2);

        %Fl{l,s,1,1} = activations(net1,slc_sz1,'pool1'); % 112x112x64
        %Fl{s,1,2} = activations(net1,slc_sz1,'pool2'); % 56x56x128
        %Fl{s,1,3} = activations(net1,slc_sz1,'pool3'); % 28x28x256
        
        Fl{l,s,2,1} = activations(net2,slc_sz1,'conv1_relu');  % 112x112x64
        %Fl{s,2,2} = activations(net2,slc_sz1,'res2c_relu');  % 56x56x256
        %Fl{s,2,3} = activations(net2,slc_sz1,'res3b3_relu'); % 28x28x512
        
        %Fl{s,3,1} = activations(net3,slc_sz2,'activation_3_relu'); % 147x147x64
        %Fl{s,3,2} = activations(net3,slc_sz2,'activation_5_relu'); % 71x71x192
        %Fl{s,3,3} = activations(net3,slc_sz2,'mixed0');            % 35x35x256        
        
        %Fl{l,s,4,1} = activations(net4,slc_sz2,'activation_3'); % 147x147x64
        %Fl{s,4,2} = activations(net4,slc_sz2,'activation_5'); % 71x71x192
        %Fl{s,4,3} = activations(net4,slc_sz2,'mixed_5b');     % 35x35x320        

        %Fl{l,s,5,1} = activations(net5,slc_sz1,'conv1|relu');   % 112x112x64
        %Fl{s,5,2} = activations(net5,slc_sz1,'pool2_conv');   % 56x56x128
        %Fl{s,5,3} = activations(net5,slc_sz1,'pool3_conv');   % 28x28x256        

        %Fl{s,6,1} = activations(net6,slc_sz1,'block_1_expand_relu'); % 112x112x96
        %Fl{s,6,2} = activations(net6,slc_sz1,'block_3_expand_relu'); % 56x56x144
        %Fl{s,6,3} = activations(net6,slc_sz1,'block_6_expand_relu'); % 28x28x192 

        %Fl{l,s,7,1} = activations(net7,slc_sz2,'block1_conv2_act'); % 147x147x64
        %[]
        %[]
    end 
    %{
    Cl = cell(8,7,3); % (#slices,#nets,#layers)
    for s = 1:1
        for n = 2 % 1:7
            for ly = 1 %1:3
                szCov = size(Fl{s,n,ly},3);
                Cov = zeros(szCov,szCov);
                Do = double(Fl{s,n,ly});
                Do = Do/max(Do(:));
                Do = reshape(Do,[],szCov); 
                muDo = mean(Do,1);
                D = Do - repmat(muDo,size(Do,1),1);
                Cov = (D'*D)/size(Do,1);
                Cl{s,n,ly} = Cov;
            end
        end
    end
    save(listing(l).name,'slices_e','Cl')  
    %save([listing(l).name(1:end-4),'_Clnews','.mat'],'Cl')
    %}
end
toc
%% Analyzing Eigenvalues
clear all, clc
addpath('data3\DER\'), cd data3\DER\
%addpath('data3\IZQ\') ,cd data3\IZQ\
%addpath('results3\DER\') ,cd results3\DER\
%addpath('results3\IZQ\') ,cd results3\IZQ\
listing = dir('*.mat'); cd ..\.. % Cl{#slices,#nets,#layers}
%==========================================================================
%{
for l = 1:2:259
    disp(l)
    load(listing(l).name)
    Cl0 = Cl; descriptor0 = descriptor; concats0 = concats; 
    load(listing(l+1).name)
    Cl(:,1:6,:) = Cl0;
    descriptor(:,1:6,:) = descriptor0;
    concats(1:6,:,:) = concats0;
    delete(listing(l+1).name)
    save(listing(l).name,'slices','slices_e','Cl','descriptor','concats')     
    clearvars -except listing l
end
%}
%==========================================================================
addpath('data3\DER\'), cd data3\DER\
listingD = dir('*.mat'); cd ..\..
addpath('results3\DER\') ,cd results3\DER\
listingR = dir('*.mat'); cd ..\..
v = 66;
figure
n = [1 2 4 7 5]; %15 20 40 20 15
tl = {'\textbf{(a) VGG-19}','\textbf{(b) ResNet-101}',...
      '\textbf{(c) Inception-ResNet-v2}',...
      '\textbf{(d) Xception}','\textbf{(e) DenseNet-201}'};
for i = 1:5 
    subplot(2,5,i)
    
    load(listingD(v).name,'Cl')
    A = flip(eig(Cl{1,n(i),1})); 
    An = cumsum(100*A/sum(A));
    kidx=0; c=0;
    for k = 1:64
        if (An(k)>=95 && c==0), kidx = k; c=1; end 
    end
    plot(An,'b','LineWidth',1), xline(kidx,'-.b','LineWidth',1,'HandleVisibility','off');
    tk = ['$k = ',num2str(kidx),'$'];
    text(kidx+1,60,tk,'Color','b','interpreter','latex')
    xlim([1 64]), ylim([0 100])
    hold on
    load(listingR(v).name,'Cl')
    A = flip(eig(Cl{1,n(i),1})); 
    An = cumsum(100*A/sum(A));
    kidx=0; c=0;
    for k = 1:64
        if (An(k)>=95 && c==0), kidx = k; c=1; end 
    end
    plot(An,'r','LineWidth',1), xline(kidx,'-.r','LineWidth',1,'HandleVisibility','off'); 
    tk = ['$k = ',num2str(kidx),'$'];
    text(kidx+1,40,tk,'Color','r','interpreter','latex')
    
    set(gca,'TickLabelInterpreter','latex','FontSize',10)
    ylabel('Explained Variance (\%)','interpreter','latex','FontSize',10)
    title(tl{i},'interpreter','latex','FontSize',10)
    xlabel('$\lambda_{i\,=\,1,\,2,\,\cdots,\,C.}$','interpreter','latex','FontSize',13.5)
end
legend('Standard','Magnified','interpreter','latex','FontSize',9)
savePDFfig('eigenvalues')
%==========================================================================
kcortes = zeros(length(listing),4,7,3);
lclrs = {'-r','-.c',':m','--k'};
for l = 1:length(listing)
    disp(l)
    load(listing(l).name)
    %{
    figure
    for ly = 1:1 % 1:3
        for n = 7:7 % 1:7
            subplot(3,7,n+(ly-1)*7)
            for s = 1:4
                plot(flip(eig(Cl{s,n,ly})),'.')
                title(sprintf('net%d-l%d',n,ly))
                hold on
            end 
        end
    end
    %}
    %figure %<=====
    for ly = 1:1 % 1:3
        for n = 7:7 % 1:7
            %subplot(3,7,n+(ly-1)*7) %<=====
            for s = 1:4
                CSumVector = cumsum(flip(eig(Cl{s,n,ly}))); kidx = 0; c=0;
                TSum = CSumVector(end); kSum = 0.95*TSum;
                for i = 1:numel(CSumVector)
                   if ((CSumVector(i)>= kSum) && c==0), kidx = i; c=1; end 
                end
                kcortes(l,s,n,ly) = kidx; 
                %plot(CSumVector,lclrs{s}), xline(kidx,lclrs{s}); hold on %<=====
            end
            %title(sprintf('net%d-l%d kmax = %d',n,ly,max(kcortes(l,:,n,ly)))) %<=====
        end
    end    
end
kcortes_maxkslc = max(kcortes,[],2); % (#listing,max_kslice,#nets,#layers)
figure
for ly = 1:1 % 1:3
    for n = 7:7 % 1:7
        subplot(3,7,n+(ly-1)*7)
        plot(kcortes_maxkslc(:,1,n,ly),'.'), ylim([0 250])
        kmax = max(kcortes_maxkslc(:,1,n,ly));
        kmin = min(kcortes_maxkslc(:,1,n,ly));
        kavg = mean(kcortes_maxkslc(:,1,n,ly));
        %title(sprintf('net%d-l%d kmax = %d kmin = %d',n,ly,kmax,kmin))
        title(sprintf('avg = %1.1f mx = %d mn = %d',kavg,kmax,kmin))
    end
end

%% PCA covariance reduction & descriptor generation
clear all, clc
pcaszs = [15 20 40 40 15 25 20; 
          70 120 95 110 45 35 NaN;
          60 210 120 55 95 50 NaN]; % pcaszs(#layers,#nets)
%addpath('data3\DER\'), cd data3\DER\
%addpath('data3\IZQ\'), cd data3\IZQ\
%addpath('results3\DER\'), cd results3\DER\
addpath('results3\IZQ\'), cd results3\IZQ\
listing = dir('*.mat'); cd ..\.. % Cl{#slices,#nets,#layers} '*_newSlcs.mat'
tic
for l = 1:1 %length(listing)
    disp(l)
    load(listing(l).name)
    Clr = cell(8,7,3); % (#slices,#nets,#layers)
    descriptor = cell(8,7,3);
    for s = 1:4 % 1:8
        for n = 2 % 1:7
            for ly = 1 % 1:3
                [Wk,Ak] = eigs(Cl{s,n,ly},pcaszs(ly,n));
                Cpca = Wk'*Cl{s,n,ly}*Wk;
                [Wpca,Apca] = eig(Cpca);
                Cpcae = Wpca*logm(Apca)*Wpca';
                Clr{s,n,ly} = Cpcae;
                Tri = tril(ones(pcaszs(ly,n),pcaszs(ly,n)));
                Tri(Tri==0) = NaN;
                ClrTri = Tri.*Clr{s,n,ly};
                descriptor{s,n,ly} = rmmissing(ClrTri(:));    
            end
        end   
    end 
    concats = cell(7,3,5); % (#nets,#layers,#concatenations)
    for n = 2 % 1:7
        for ly = 1:1 % 1:3
            concats{n,ly,1} = descriptor{1,n,ly}; % 1 slices (S1)
            concats{n,ly,2} = [descriptor{2,n,ly}; descriptor{4,n,ly}]; % 2 slices (S2,S4)
            concats{n,ly,3} = [descriptor{1,n,ly}; descriptor{2,n,ly};...
                               descriptor{3,n,ly}; descriptor{4,n,ly}]; % 4 slices
            %concats{n,ly,4} = [descriptor{1,n,ly}; descriptor{2,n,ly};...
            %                   descriptor{3,n,ly}; descriptor{4,n,ly};...
            %                   descriptor{6,n,ly}; descriptor{7,n,ly}]; % 6 slices 
            %concats{n,ly,5} = [descriptor{1,n,ly}; descriptor{2,n,ly};...
            %                   descriptor{3,n,ly}; descriptor{4,n,ly};...
            %                   descriptor{6,n,ly}; descriptor{7,n,ly};...
            %                   descriptor{5,n,ly}; descriptor{8,n,ly}]; % 8 slices                           
                           
        end
    end   
    %save(listing(l).name,'slices,'slices_e','Cl','descriptor','concats')
    %save([listing(l).name(1:end-4),'.mat'],'Cl','descriptor','concats')
    
    %save(listing(l).name,'Cl','descriptor','concats')
end
toc

%% Stereo Concatenation
clear all, clc
addpath('data3\DER\'), cd data3\DER\
%addpath('results3\DER\') ,cd results3\DER\
listing = dir('*_newSlcs.mat'); cd ..\..
DataDER = cell(7,3,5);
for n = 2 % 1:7
    for ly = 1 
        for c = 1:5 
            fprintf('n%d ly%d c%d =========\n',n,ly,c)
            clear Data
            for l = 1:length(listing)
                load(listing(l).name,'concats')
                Data(l,:) = concats{n,ly,c}';
            end
            DataDER{n,ly,c} = Data;
        end
    end
end
addpath('data3\IZQ\') ,cd data3\IZQ\
%addpath('results3\IZQ\') ,cd results3\IZQ\
listing = dir('*_newSlcs.mat'); cd ..\..
DataIZQ = cell(7,3,5);
for n = 2 % 1:7
    for ly = 1
        for c = 1:5 
            fprintf('n%d ly%d c%d =========\n',n,ly,c)
            clear Data
            for l = 1:length(listing)
                load(listing(l).name,'concats')
                Data(l,:) = concats{n,ly,c}';
            end
            DataIZQ{n,ly,c} = Data;
        end
    end
end
DataSTR = cell(7,3,5);
for n = 2:2 % 1:7
    for ly = 1:1 
        for c = 1:5     
            DataSTR{n,ly,c} = [DataDER{n,ly,c} DataIZQ{n,ly,c}];
        end
    end
end

%% SVM classification 
clear all, clc

load('data3_feedSVM_new.mat')
Data = DataDER; clear DataDER
Data = DataIZQ; clear DataIZQ
Data = DataSTR; clear DataSTR

load('results3_feedSVM_new.mat')
Data = DataDER; clear DataDER
Data = DataIZQ; clear DataIZQ
Data = DataSTR; clear DataSTR

idx = cell(26,1); c = 1;
for p = 1:26
    for s = 1:5
        idx{p}(s) = c;
        c = c+1;
    end
end
labels = [zeros(65,1);ones(65,1)]; clear p s c

DataEval = cell(7,3,5);
for n = 2 % #nets 1:7
    for ly = 1 % #layers 1:3
        for c = 3 % [1 4 5] % #concatenations 1:3 => S1, S2-U-S4, ALL
            fprintf('net%d - ly%d - c%d\n',n,ly,c)
            klabels = cell(26,1); kscores = cell(26,1);
            for k = 1:1 %parfor k = 1:26
                tic
                fprintf('fold %d\n',k)
                Test = Data{n,ly,c}(idx{k},:);
                Train = Data{n,ly,c}(setdiff(1:130,idx{k}),:);
                TestLabels = labels(idx{k}); 
                TrainLabels = labels(setdiff(1:130,idx{k}));
                rng default
                CVmodel = fitcsvm(Train,TrainLabels,'KernelFunction','RBF',...
                          'OptimizeHyperparameters',{'BoxConstraint','KernelScale','Standardize'},...
                          'HyperparameterOptimizationOptions',...
                                struct('AcquisitionFunctionName','expected-improvement-plus',...
                                'UseParallel',false,'Repartition',true,'kfold',5,'ShowPlots',false,'Verbose',0));        
                PPCVmodel = fitSVMPosterior(CVmodel);
                toc
                tic
                [klabels{k},kscores{k}] = predict(PPCVmodel,Test); 
                toc
            end 
            FoldsEval.klabels = klabels;
            FoldsEval.kscores = kscores;
            DataEval{n,ly,c} = FoldsEval;
            pause(1)
            fprintf('acc = %0.1f\n',getAcc(DataEval,n,ly,c))
        end
    end
end
%save('DataEval_results3_STRnews.mat','DataEval')

%% Printing & Plotting Results
clear all, clc

%==========================================================================
load DataEval_data3_DER.mat, data3_DER = DataEval;
load DataEval_results3_DER.mat, results3_DER = DataEval;
clear DataEval
%==========================================================================
load DataEval_data3_IZQ.mat, data3_IZQ = DataEval;
load DataEval_results3_IZQ.mat, results3_IZQ = DataEval;
clear DataEval
%==========================================================================
load DataEval_data3_STR.mat, data3_STR = DataEval;
load DataEval_results3_STR.mat, results3_STR = DataEval;
clear DataEval
%==========================================================================
gtlabels = [zeros(65,1);ones(65,1)]; 
tl = {'\textbf{(a) VGG-19}','\textbf{(b) ResNet-101}',...
      '\textbf{(c) Inception-ResNet-v2}',...
      '\textbf{(d) Xception}','\textbf{(e) DenseNet-201}'};
lw = 1.2;
n = [1 2 4 7 5];
dstring = {'data3_STR','data3_STR','data3_DER',...
           'data3_IZQ','data3_DER'}; 
rstring = {'results3_STR','results3_STR','results3_DER',...
           'results3_IZQ','results3_DER'}; 
figure
for i = 1:5
    subplot(2,5,i)   
    eval(['[Xd,Yd,AUCd(i),~] = getROC(',dstring{i},',n(i),1,3,gtlabels);'])
    plot(Xd,Yd,'b','LineWidth',lw), hold on
    tk = ['$AUC_{std} = ',num2str(AUCd(i),'%0.3f'),'$'];
    text(0.425-0.2,0.3,tk,'Color','b','interpreter','latex')
    eval(['[Xr,Yr,AUCr(i),~] = getROC(',rstring{i},',n(i),1,3,gtlabels);'])
    plot(Xr,Yr,'r','LineWidth',lw), title(tl{i},'interpreter','latex')
    set(gca,'TickLabelInterpreter','latex')
    tk = ['$AUC_{mag} = ',num2str(AUCr(i),'%0.3f'),'$'];
    text(0.4-0.2,0.15,tk,'Color','r','interpreter','latex')
    xlabel('FPR','interpreter','latex')
    ylabel('TPR','interpreter','latex')
end
legend('Standard','Magnified','interpreter','latex','FontSize',9)
savePDFfig('ROCs')
close all

%==========================================================================
load DataEval_data3_DER.mat, de_data3 = DataEval;
load DataEval_results3_DER.mat, de_results3 = DataEval;
clear DataEval
%==========================================================================
load DataEval_data3_IZQ.mat, de_data3 = DataEval;
load DataEval_results3_IZQ.mat, de_results3 = DataEval;
clear DataEval
%==========================================================================
load DataEval_data3_STR.mat, de_data3 = DataEval;
load DataEval_results3_STR.mat, de_results3 = DataEval;
clear DataEval
%==========================================================================
roc_cl = {[0.3010 0.7450 0.9330], [0 0 1];... % B: 1S_std 1S_mag
          [0.4660 0.6740 0.1880], [0 1 0];... % G: 2S_std 2S_mag
          [0.8500 0.3250 0.0980], [1 0 0]};   % R: 4S_std 4S_mag
xlb = {'1S','2S','4S'};
gtlabels = [zeros(65,1);ones(65,1)]; 
for n = 1:7 % 1:7
    for ly = 1:1 % 1:3
        subplot(3,7,n+(ly-1)*7)
        %{
        for c = 3:3 % 1:3            
            acc_data3(c) = getAcc(de_data3,n,ly,c);
            acc_results3(c) = getAcc(de_results3,n,ly,c);
        end
        fprintf('net%d d3: %0.1f\n',n,acc_data3(3))
        fprintf('net%d r3: %0.1f\n',n,acc_results3(3))
        dbar = [acc_data3' acc_results3'];
        bar(dbar), ylim([45 95]), grid on
        set(gca,'XTickLabel',xlb), set(gca,'YTick',45:5:95)
        %}
        %
        for c = 1:3 % 1:3
            [Xd,Yd,AUCd(c),~] = getROC(de_data3,n,ly,c,gtlabels);
            plot(Xd,Yd,'LineWidth',1,'Color',roc_cl{c,1}), hold on
        end
        for c = 1:3 % 1:3
            [Xr,Yr,AUCr(c),~] = getROC(de_results3,n,ly,c,gtlabels);
            plot(Xr,Yr,'LineWidth',1,'Color',roc_cl{c,2}), grid on
        end
        lg = legend(sprintf('1S std: %0.3f',AUCd(1)),sprintf('2S std: %0.3f',AUCd(2)),sprintf('4S std: %0.3f',AUCd(3)),...
                    sprintf('1S mag: %0.3f',AUCr(1)),sprintf('2S mag: %0.3f',AUCr(2)),sprintf('4S mag: %0.3f',AUCr(3)),...
                    'Location','southeast','NumColumns',2,'FontSize',8);
        lg.ItemTokenSize = [10 20];
        xlabel('FPR'), ylabel('TPR')
        %}
        title(sprintf('net%d layer%d',n,ly))
    end
end
legend('Standard','Magnified','fontsize',10)
savePDFfig('acc')
savePDFfig('roc')
%bar(diag(acc),'stacked')


%%
x = zeros(15,1);
y1 = zeros(15,1);
y2 = zeros(15,1);

plot(x,y1,'-d',x,y2,'-*') % Selected scale: 0.35 !!!
grid off
axis([1 15 0 14])
ylbl = '\begin{tabular}{c} No. of Layer \\ Accuracies \end{tabular}';

ylabel(ylbl,'interpreter','latex')
set(gca,'ytick', 2:2:12,'XTickLabel',2:2:12,'TickLabelInterpreter','latex');

xlabel(['$f$',' [Hz]'],'interpreter','latex')
set(gca,'xtick', 1:15,'XTickLabel',1:15,'TickLabelInterpreter','latex');

set(gca,'fontsize',12), hold on

shade([4 5 6 7],[15 15 15 15],'FillColor',[147 196 227]/255,'FillAlpha',0.3)

legend('Increased','Reduced','interpreter','latex','fontsize',10)


savePDFfig('freqMag')

{'-p','-o','-*','-x','-s','-d'}


%%
clear all, clc

slc = imread('PK03_DER_S3_a15_crop.png');
net1 = vgg19;             %sz1
net2 = resnet101;         %sz1
net4 = inceptionresnetv2; %sz2
net5 = densenet201;       %sz1
net7 = xception;          %sz2
sz1 = [224,224]; sz2 = [299,299];
slc_sz1 = imresize(slc,sz1);
slc_sz2 = imresize(slc,sz2);
F{1} = activations(net1,slc_sz1,'pool1'); % 112x112x64
F{2} = activations(net2,slc_sz1,'conv1_relu');  % 112x112x64
F{3} = activations(net4,slc_sz2,'activation_3'); % 147x147x64
F{4} = activations(net7,slc_sz2,'block1_conv2_act'); % 147x147x64
F{5} = activations(net5,slc_sz1,'conv1|relu');   % 112x112x64

titlelbs = {'\textbf{(a) VGG-19}',...
            '\textbf{(b) ResNet-101}',...
            '\textbf{(c) Inception-ResNet-v2}',...
            '\textbf{(d) Xception}',...
            '\textbf{(e) DenseNet-201}'};
r(1,:) = [13 2 10 53]; %  1
r(2,:) = [44 50 58 6]; % 55 
r(3,:) = [43 01 47 40];
r(4,:) = [30 64 11 58];  %49
r(5,:) = [7 49 62 54];  %57 49
sx = 60;
fsz = 14;
alpch = zeros(140+2*sx,140+2*sx); alpch((sx+1):(140+sx),(sx+1):(140+sx)) = 1;
slca = zeros(140+2*sx,140+2*sx,3,'uint8'); slca((sx+1):(140+sx),(sx+1):(140+sx),:) = slc;

figure,
ha = tight_subplot(1,6,.02,.05,.05)
axes(ha(1)), image(slca,'AlphaData',alpch); axis image, set(gca,'visible','off')
for i = 1:5
    axes(ha(i+1))
    features = F{i};
    szf = size(features);  
    fch = reshape(features,[],szf(3));
    rfeatures = reshape(features,[szf(1) szf(2) 1 szf(3)]);
    sfeatures = rfeatures(:,:,:,r(i,:));
    gridf = imtile(mat2gray(sfeatures),'GridSize',[2 2]);
    fprintf('min: %0.3f - max:%0.3f\n',min(min(gridf)),max(max(gridf)))
    imshow(gridf,[])
    title(titlelbs{i},'interpreter','latex')
    set(gca,'fontsize',fsz) 
end
colormap jet
colorbar('TickLabelInterpreter','latex')

savePDFfig('cnnFeatures')



for i = 1:64  
   imshow(F{5}(:,:,i),[])
   colormap jet
   saveas(gcf,sprintf('%02d.png',i))
end


%%
