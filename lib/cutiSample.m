function cutiSample(vName,isample)
iFrames = {[1 300],     % Sample 1
           [301 600],   % Sample 2
           [601 900],   % Sample 3
           [901 1200],  % Sample 4
           [1201 1500]};% Sample 5

Vsbjct = VideoReader(vName);
vName_smpl = [vName(1:end-4),'_',sprintf('S%d',isample),'.avi'];
Vsbjct_ismpl = VideoWriter(vName_smpl,'Uncompressed AVI');
Vsbjct_ismpl.FrameRate = Vsbjct.FrameRate;

open(Vsbjct_ismpl)
frames = read(Vsbjct,iFrames{isample});
writeVideo(Vsbjct_ismpl,frames)
close(Vsbjct_ismpl)

end


