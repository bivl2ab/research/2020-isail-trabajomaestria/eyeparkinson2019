function vector = str2vector(str)
    for i = 1:length(str)
        vector(i) = str2num(str(i));
    end
   
    
end