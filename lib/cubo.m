% M = repmat(im2frame(imread('eye.png')),1,200);  
% M = repmat(im2frame(F(1).data),1,150);
% 
for k = 1:300
   Fa(:,:,:,k) = F(k).data; 
end
FaR = uint8(mean(Fa(:,:,1,:),4));
FaG = uint8(mean(Fa(:,:,2,:),4));
FaB = uint8(mean(Fa(:,:,3,:),4));
FaRGB(:,:,1) = FaR; 
FaRGB(:,:,2) = FaG;
FaRGB(:,:,3) = FaB;
subplot(1,2,1), imshow(FaRGB), title('Average Magnified Frame')
subplot(1,2,2), imshow(F(1).data), title('First Frame')

%%
clear all
vidObj = VideoReader('PK2.avi');
k = 1;
while hasFrame(vidObj)
    F(k).data = readFrame(vidObj);
    k = k+1;
end
F(300).data = F(299).data; 

imshow(F(1).data)

%%
clear M
for k = 1:110 %300
   M(k) = im2frame(F(k).data); 
end

nFrames = numel(M);                  
face1 = frame2im(M(1));              
[R,C,D] = size(face1);              
face2 = zeros(R,nFrames,3,'uint8');  
face3 = zeros(nFrames,C,3,'uint8'); 
for k = 1:nFrames              
  img = frame2im(M(k));        
  face2(:,k,:) = img(:,end,:);  
  face3(k,:,:) = img(1,:,:);    
end
offset = nFrames/sqrt(2);  
figure()
surf([0 C; 0 C],[R R; 0 0],[0 0; 0 0],...
    'FaceColor','texturemap','CData',face1);
hold on                         
surf([C C+offset; C C+offset],[R R+offset; 0 offset],...
     [0 0; 0 0],'FaceColor','texturemap','CData',face2);
surf([0 C; offset C+offset],[R R; R+offset R+offset],...
     [0 0; 0 0],'FaceColor','texturemap','CData',face3);
axis equal                    
view(2)                      
axis off                     
set(gcf,'Color','w','Position',[50 50 C+offset+20 R+offset+20]);
set(gca,'Units','pixels','Position',[10 10 C+offset R+offset]);
