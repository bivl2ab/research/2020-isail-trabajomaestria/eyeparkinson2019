function [macc,stdacc] = getAcc(DataEval,n,ly,c)
%{
klabelsC = DataEval{n,ly,c}.klabels(1:13);
klabelsC = cat(1,klabelsC{:});
FP = nnz(klabelsC); TN = 65 - FP;

klabelsPK = DataEval{n,ly,c}.klabels(14:26); 
klabelsPK = cat(1,klabelsPK{:});
FN = nnz(~klabelsPK); TP = 65 - FN;

acc = 100*(TP+TN)/(TP+TN+FP+FN);
%}

% klabels = DataEval{n,ly,c}.klabels;
% klabels = cat(1,klabels{:});
% gtlabels = [zeros(65,1);ones(65,1)]; 
% acc2 = sum(klabels == gtlabels)/numel(gtlabels);
% fprintf('%0.1f\n',100*acc2)

klabels = DataEval{n,ly,c}.klabels;
gtlabels = cell(26,1);
for i=1:13, gtlabels{i} = zeros(5,1); end
for i=14:26, gtlabels{i} = ones(5,1); end
for i=1:26, kacc(i) = sum(klabels{i} == gtlabels{i})/5; end
macc = 100*mean(kacc);
stdacc = 100*std(kacc);

end


