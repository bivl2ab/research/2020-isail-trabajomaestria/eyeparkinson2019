function [X,Y,AUC,kscores] = getROC(DataEval,n,ly,c,gtlabels);

kscores = DataEval{n,ly,c}.kscores; 
kscores = cat(1,kscores{:});
[X,Y,T,AUC] = perfcurve(gtlabels,kscores(:,2),1);

end