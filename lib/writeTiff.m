function writeTiff(name,imgdata)

t = Tiff(name,'w');
tagstruct.ImageLength = 210;
tagstruct.ImageWidth = 300;
tagstruct.Photometric = Tiff.Photometric.Separated;
tagstruct.BitsPerSample = 8;
tagstruct.SamplesPerPixel = 4;
tagstruct.PlanarConfiguration = Tiff.PlanarConfiguration.Separate;
setTag(t,tagstruct)
write(t,imgdata)
close(t)

end