function S = getSlice(file,mode,pts)
% (2018) By IsailSlzr 
% 
% Son 3 modos, en todos los casos el programa le grafica y le 
% guarda el slice como imagen :
% 
% S = getSlice('pk1.avi','diagonal')
% Ac� le va a mostrar un frame y le va a pedir de qu� punto a 
% qu� punto quiere el slice (da 2 clicks izq. para formar la 
% linea y termina con click der.)
% 
% S = getSlice('pk1.avi','vertical')
% Ac� igualmente le muestra un frame pero ahora solo le pide 
% un punto (da 1 click izq. y termina con click der.), y el 
% programa le grafica todo el slice vertical que pasa por ese punto.
% 
% S = getSlice('pk1.avi','horizontal')
% Ac� igualmente le muestra un frame y le pide un punto (da 1 
% click izq. y termina con click der.), y el programa le grafica 
% todo el slice horizontal que pasa por ese punto.

vidObj = VideoReader(file);
k = 1;
while hasFrame(vidObj)
    F(k).data = readFrame(vidObj);
    k = k+1;
end   
%figure(1),imshow(F(1).data,'InitialMagnification','fit')

if strcmp(mode,'diagonal')
    %[xi,yi] = getline;
    %P1 = round([xi(1),yi(1)])
    %P2 = round([xi(2),yi(2)])    
    x1 = pts.P1(1); y1 = -pts.P1(2);
    x2 = pts.P2(1); y2 = -pts.P2(2);
    xSz = size(F(1).data,2);
    x = linspace(x1,x2,xSz);
    y = round(((y2-y1)/(x2-x1))*(x - x1) + y1);
    x = round(x);
    S = zeros(xSz,3,k-1,'uint8');
    for i = 1:k-1
       Fi = F(i).data;
       for j = 1:length(x)
           S(j,:,i) = Fi(-y(j),x(j),:);
       end
    end   
    S = permute(S,[1 3 2]);
    if (y2-y1)/(x2-x1) > 0
        S = flip(S);  
    end    
    %sufix = sprintf('_slice(%d,%d)to(%d,%d).png',P1(1),P1(2),P2(1),P2(2));
    %imwrite(S,strcat(file(1:end-4),sufix))
end

if strcmp(mode,'vertical')
    %[xi,yi] = getpts; 
    %xPixel = round(xi(1))
    xPixel = pts.xP;
    S = zeros(size(F(1).data,1),3,k-1,'uint8');
    for i = 1:k-1
        Fi = F(i).data;
        S(:,:,i) = squeeze(Fi(:,xPixel,:));   
    end
    S = permute(S,[1 3 2]);
    %sufix = sprintf('_sliceYT(x=%d).png',xPixel);
    %imwrite(S,strcat(file(1:end-4),sufix))
end

if strcmp(mode,'horizontal')
    %[xi,yi] = getpts; 
    %yPixel = round(yi(1))
    yPixel = pts.yP;
    S = zeros(size(F(1).data,2),3,k-1,'uint8');
    for i = 1:k-1
       Fi = F(i).data;
       S(:,:,i) = squeeze(Fi(yPixel,:,:));   
    end
    S = permute(S,[3 1 2]);
    %sufix = sprintf('_sliceXT(y=%d).png',yPixel);
    %imwrite(S,strcat(file(1:end-4),sufix))  
end    

%figure(2),imshow(S)
      
end