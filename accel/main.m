%% Running
[vid_in,params] = setparameters('myEye','.avi','data','results_accel',2.5,15,'DOG');
motionamp(vid_in,params);


%% Examples
%%% Synehtic ball video %%%%%%%%%%%%%%%%%%%%%%%%%%
[vid_in,params] = setparameters('syn_ball', '.avi','data','results_accel',10/3,5,'DOG');
motionamp(vid_in,params);

%%% Cat toy video %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
[vid_in,params] = setparameters('cat_toy','.mp4','data','results_accel',4,8,'INT');
motionamp(vid_in,params);

%%% Gun shot video %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
[vid_in,params] = setparameters('gun_shot','.mp4','data','results_accel',8,8,'DOG');
motionamp(vid_in,params);

%%% Parkinson I video %%%%%%%%%%%%%%%%%%%%%%%%%%%% 
[vid_in,params] = setparameters('parkinson1','.mp4','data','results_accel',3,8,'INT');
motionamp(vid_in,params);

%%% Parkinson II video %%%%%%%%%%%%%%%%%%%%%%%%%%% 
[vid_in,params] = setparameters('parkinson2','.mp4','data','results_accel',3,8,'INT');
motionamp(vid_in,params);

%%% Bottle video %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
[vid_in,params] = setparameters('bottle_moving','.mp4','data','results_accel',4,8,'INT');
motionamp(vid_in,params);

%%% Eye video %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
[vid_in,params] = setparameters('eye_raw','.mp4','data','results_accel',2.5,15,'DOG');
motionamp(vid_in,params);


