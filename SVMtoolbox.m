clear all, clc
dtype = 'results2';

layers = {'net1_layerA',...   %1
          'net1_layerB',...   %2
          'net2_layerA',...   %3
          'net2_layerB',...   %4
          'net3_layerA',...   %5
          'net3_layerB'};     %6
labels = {'00','11','00','11',...
          '00','11','00','11',...
          '00','11','00','11'}; 
K = 12;
NumSlices = 4; ly = 1; 
%{
% C1 Si_1       
% C1 Si_2
% PK6 Si_1     
% PK6 Si_2
% C2 Si_1
% C2 Si_2
% PK5 Si_1     
% PK5 Si_2
% C3 Si_1      
% C3 Si_2
% PK4 Si_1     
% PK4 Si_2
% C4 Si_1 
% C4 Si_2
% PK3 Si_1     
% PK3 Si_2
% C5 Si_1  
% C5 Si_2
% PK2 Si_1     
% PK2 Si_2
% C6 Si_1      
% C6 Si_2
% PK1 Si_1      
% PK1 Si_2
%}
%%
SVMresults = cell(4,6,2); %(NumSlices,ly,[klabels,kscores])
for NumSlices = [1 2 4]
    disp(sprintf('NumSlices: %d',NumSlices))
    for ly = 1:6
        disp(sprintf('Layer: %d',ly))
        %cd([dtype,'_features'])
        load([dtype,'_',layers{ly},'.mat'])
        %cd ..
        ktest = cell(12,1); ktrain = cell(12,1); 
        for k = 1:K
            if NumSlices == 1, eval(['concatS_Ktest = [',dtype,'.S2.Test.k',num2str(k),'];']), end
            if NumSlices == 2, eval(['concatS_Ktest = [',dtype,'.S1.Test.k',num2str(k),...
                                                     ',',dtype,'.S3.Test.k',num2str(k),'];']), end
            if NumSlices == 4, eval(['concatS_Ktest = [',dtype,'.S1.Test.k',num2str(k),...
                                                     ',',dtype,'.S2.Test.k',num2str(k),...
                                                     ',',dtype,'.S3.Test.k',num2str(k),...
                                                     ',',dtype,'.S4.Test.k',num2str(k),'];']), end                            
            if NumSlices == 1, eval(['concatS_Ktrain = [',dtype,'.S2.Train.k',num2str(k),'];']), end
            if NumSlices == 2, eval(['concatS_Ktrain = [',dtype,'.S1.Train.k',num2str(k),...
                                                      ',',dtype,'.S3.Train.k',num2str(k),'];']), end   
            if NumSlices == 4, eval(['concatS_Ktrain = [',dtype,'.S1.Train.k',num2str(k),...
                                                      ',',dtype,'.S2.Train.k',num2str(k),...
                                                      ',',dtype,'.S3.Train.k',num2str(k),...
                                                      ',',dtype,'.S4.Train.k',num2str(k),'];']), end
                 Ktest{k} = concatS_Ktest;
                 Ktrain{k} = concatS_Ktrain;        
        end
        clear data2 results2 concatS_Ktest concatS_Ktrain
        klabels = cell(12,1); kscores = cell(12,1); 
        for k = 1:K
            disp(k)
            idxlb = setdiff(1:K,k); Tlabels = strcat(labels{idxlb});
            rng default
            CVmodel = fitcsvm(Ktrain{k},str2vector(Tlabels)','KernelFunction','RBF',...
                      'OptimizeHyperparameters',{'BoxConstraint','KernelScale','Standardize'},...
                      'HyperparameterOptimizationOptions',...
                            struct('AcquisitionFunctionName','expected-improvement-plus',...
                            'UseParallel',true,'Repartition',true,'kfold',5,'ShowPlots',false,'Verbose',0));        
            PPCVmodel = fitSVMPosterior(CVmodel);
            [klabels{k},kscores{k}] = predict(PPCVmodel,Ktest{k});
        end
        SVMresults{NumSlices,ly,1} = klabels;
        SVMresults{NumSlices,ly,2} = kscores;
        clear CVmodel PPCVmodel idxlb Tlabels
    end
end

%% ROC AUC Curves
clear all, clc
%load('SVMresults.mat')

%markers = {'-p','-o','-*','-x','-s','-d'};
layers = {'Net 1 LayerA',...   %1
          'Net 1 LayerB',...   %2
          'Net 2 LayerA',...   %3
          'Net 2 LayerB',...   %4
          'Net 3 LayerA',...   %5
          'Net 3 LayerB'};     %6 
isubplots = [1 4 2 5 3 6];      
ly = 1;
figure,
for ly = 1:6
    ly_kscores = SVMresults([1 2 4],ly,2);
    for nS = 1:3
        concat_kscores = [];
        for k = 1:12
           concat_kscores = [concat_kscores; ly_kscores{nS}{k}];
        end
        ly_kscores{nS} = concat_kscores;
    end
    gtlabels = repmat([0,0,1,1],[1,6])';
    markers = {'-o','-*','-p'};
    subplot(2,3,isubplots(ly))
    for nS = 1:3
        [X,Y,T,AUC(nS)] = perfcurve(gtlabels,ly_kscores{nS}(:,2),1);
        plot(X,Y,markers{nS},'LineWidth',1), hold on
    end
    legend(sprintf('S1: %0.3f',AUC(1)),sprintf('S2: %0.3f',AUC(2)),...
           sprintf('S4: %0.3f',AUC(3)),'Location','southeast')
    xlabel('False positive rate'), ylabel('True positive rate')
    title(layers{ly}) 
end

%%
fig = gcf;
fig.PaperPositionMode = 'manual';
orient(fig,'landscape')
print(fig,'LandscapePage_ExpandedFigure.pdf','-dpdf')


% [B,~,stats] = glmfit(zscore([scores_pd, scores_age]), Y, 'binomial', 'link', 'logit');
% B_lo = B(2)-1.965*stats.se(2);  %Lower bound
% B_hi = B(2)+1.965*stats.se(2);  %Upper bound
% fprintf('PD:  OR=%1.3f 95%%CI: %1.3f-%1.3f, p=%1.4f\n',exp(B(2)), exp(B_lo), exp(B_hi), stats.p(2))

%Confounding => eliminarlo con el diseño del experimento
%				ajuste a nivel de diseño del experimento
%				ajuste al nivel del análisis estadistico
%				incorporar esas variables (edad) en las medidas para ver si mantienen el valor predictivo				
%CI
%p value				
%ODD ratio incluyendo edades
%glmfit => ODDs ratio
%confidence interval de curva ROC



