SET

%% Plotting Eigens 
%{
% Covariances
figure
for sj = 1:12
    Cov = net3_layerB.S1_1{3}(sj,:,:);
    strng = sprintf('%d: %1.4f - %1.4f', sj, min(Cov(:)), max(Cov(:)));
    disp(strng)
    plot(Cov(:),'.')
    pause
end
% Eigenvectors
figure
for sj = 1:12
    eV = net1_layerA.S1_1{1}{sj};
    strng = sprintf('%d: %1.4f - %1.4f', sj, min(eV(:)), max(eV(:)));
    disp(strng)
    plot(eV(:),'.')
    pause
end
% Eigenvalues
figure
subplot(1,6,1), hold on, title('VGG-19 Layer A')
for sj = 1:12
    plot(flip(diag(net1_layerA.S1_1{1}{sj})),'.')
end
xlim([0 64])
subplot(1,6,2), hold on, title('VGG-19 Layer B')
for sj = 1:12
    plot(flip(diag(net1_layerB.S1_1{1}{sj})),'.')
end
xlim([0 128])
subplot(1,6,3), hold on, title('ResNet-101 Layer A')
for sj = 1:12
    plot(flip(diag(net2_layerA.S1_1{1}{sj})),'.')
end
xlim([0 64])
subplot(1,6,4), hold on, title('ResNet-101 Layer B')
for sj = 1:12
    plot(flip(diag(net2_layerB.S1_1{1}{sj})),'.')
end
xlim([0 256])
subplot(1,6,5), hold on, title('InceptResNet-v2 Layer A')
for sj = 1:12
    plot(flip(diag(net3_layerA.S1_1{1}{sj})),'.')
end
xlim([0 64])
subplot(1,6,6), hold on, title('InceptResNet-v2 Layer B')
for sj = 1:12
    plot(flip(diag(net3_layerB.S1_1{1}{sj})),'.')
end
xlim([0 320])
%}
%{
figure

subplot(1,3,1), hold on, box on
title('\textbf{(a)} ResNet-101 Layer A','interpreter','latex')
for sj = 1:12
    plot(flip(diag(net2_layerA.S1_1{1}{sj})),'.')
end
axis([0 64 0 9])
ylabel('$\lambda_i$','interpreter','latex')
xlabel('$i = 1, 2, \cdots, C.$','interpreter','latex')
set(gca,'TickLabelInterpreter','latex','fontsize',12);
grid, grid minor

subplot(1,3,2), hold on, box on
title('\textbf{(b)} ResNet-101 Layer B','interpreter','latex')
for sj = 1:12
    plot(flip(diag(net2_layerB.S1_1{1}{sj})),'.')
end
axis([0 256 0 9])
ylabel('$\lambda_i$','interpreter','latex')
xlabel('$i = 1, 2, \cdots, C.$','interpreter','latex')
set(gca,'TickLabelInterpreter','latex','fontsize',12);
grid, grid minor

subplot(1,3,3), hold on, box on
title('\textbf{(c)} Inception-ResNet-v2 Layer B','interpreter','latex')
for sj = 1:12
    plot(flip(diag(net3_layerB.S1_1{1}{sj})),'.')
end
axis([0 320 0 9])
ylabel('$\lambda_i$','interpreter','latex')
xlabel('$i = 1, 2, \cdots, C.$','interpreter','latex')
set(gca,'TickLabelInterpreter','latex','fontsize',12);
grid, grid minor

savePDFfig('eigenvalues')
%}
%% Comparing Slice Covariances
%{
C = squeeze(net1_layerA.S1_1{5}(2,:,:));

F = ones(10,10).*1e12;
for i = 1:10
    F(i,i) = 1; 
end
squeeze(C.*F);

imagesc(C.*F)

logC = zeros(10,10);
for i = 1:10
    for j = 1:10
        if C(i,j) < 0
            logC(i,j) = -log(-C(i,j));
        elseif C(i,j) > 0    
            logC(i,j) = log(C(i,j));
        end
    end
end

imagesc(logC)
colormap hsv
%}
%% Cummulative Eigenvalues
%{
figure
hold on
for sj = 1:12
    egValSum = cumsum(diag(net3_layerB.S1_1{2}{sj})); 
    plot(egValSum,'.')
    TSum = egValSum(end); TSumP = 0.05*TSum;
    c = 0;
    for i = 1:numel(egValSum)
        if (egValSum(i) >= (TSum - TSumP)) && (c == 0)
            PC(sj) = i;
            disp(i), c = 1;
        end
    end
end
mean(PC)
%                           DATA S1_1   DATA S4_1   RESULTS S1_1    RESULTS S4_1
% net1_layerA => PC: 10     !9.333      9.250           10.33       10.75
% net1_layerB => PC: 50     !50.58      49.17           54.92       52.67
% net2_layerA => PC: 10     !7.250      7.250           8.417       7.750
% net2_layerB => PC: 30     !23.58      25.17           27.50       25.92
% net3_layerA => PC: 30     !23.83      25.33           27.67       23.92
% net3_layerB => PC: 40     !38.00      32.92           34.42       39.58
%}


%% Boxplots 
N1_LA = zeros(36,1); N1_LB = zeros(36,1);
N2_LA = zeros(36,1); N2_LB = zeros(36,1);
N3_LA = zeros(36,1); N3_LB = zeros(36,1);
N1_LAM = zeros(36,1); N1_LBM = zeros(36,1);
N2_LAM = zeros(36,1); N2_LBM = zeros(36,1);
N3_LAM = zeros(36,1); N3_LBM = zeros(36,1);
D = [N1_LA,N1_LAM,zeros(36,1),N1_LB,N1_LBM,...
     zeros(36,2),...
     N2_LA,N2_LAM,zeros(36,1),N2_LB,N2_LBM,...
     zeros(36,2),...
     N3_LA,N3_LAM,zeros(36,1),N3_LB,N3_LBM];
violinplot(D)
boxplot(D)
violin(D)
distributionPlot(D,'variableWidth','false')

D = [N1_LA,N1_LAM, N1_LB,N1_LBM,...
     N2_LA,N2_LAM, N2_LB,N2_LBM,...
     N3_LA,N3_LAM, N3_LB,N3_LBM]; 
%T = cell2table(cell(12,4), 'VariableNames', {'TP','FP','TN','FN'}, ...
T = cell2table(cell(12,12), 'VariableNames', ...
    {'FP_S1','FP_S2','FP_S4','FN_S1','FN_S2','FN_S4',...
     'TP_S1','TP_S2','TP_S4','TN_S1','TN_S2','TN_S4'}, ... 
    'RowNames',{'N1_LA','N1_LAM', 'N1_LB','N1_LBM',...
                'N2_LA','N2_LAM', 'N2_LB','N2_LBM',...
                'N3_LA','N3_LAM', 'N3_LB','N3_LBM'});
L = {'C';'PK'}; L = repmat(L,18,1);
%% Specificity and Sensitivity
for s = 1:12
    M = cell(36,2);
    for i = 25:36 % 1:36   
        if strcmp(L{i},'C') && (D(i,s) == 100)
            M{i,1} = 'TN'; M{i,2} = 'TN';
        elseif strcmp(L{i},'C') && (D(i,s) == 50)
            M{i,1} = 'TN'; M{i,2} = 'FP';
        elseif strcmp(L{i},'C') && (D(i,s) == 0)
            M{i,1} = 'FP'; M{i,2} = 'FP';

        elseif strcmp(L{i},'PK') && (D(i,s) == 100)
            M{i,1} = 'TP'; M{i,2} = 'TP';
        elseif strcmp(L{i},'PK') && (D(i,s) == 50)
            M{i,1} = 'TP'; M{i,2} = 'FN';
        elseif strcmp(L{i},'PK') && (D(i,s) == 0)
            M{i,1} = 'FN'; M{i,2} = 'FN'; 
        end
    end
    TP = 0; FP = 0; TN = 0; FN = 0;
    for i = 25:36 % 1:36
        if strcmp(M{i,1},'TP') 
            TP = TP + 1;
        elseif strcmp(M{i,1},'FP')
            FP = FP + 1;
        elseif strcmp(M{i,1},'TN')
            TN = TN + 1;
        elseif strcmp(M{i,1},'FN')    
            FN = FN + 1;
        end
        if strcmp(M{i,2},'TP') 
            TP = TP + 1;
        elseif strcmp(M{i,2},'FP')
            FP = FP + 1;
        elseif strcmp(M{i,2},'TN')
            TN = TN + 1;
        elseif strcmp(M{i,2},'FN')    
            FN = FN + 1;
        end     
    end
        %T{s,1} = num2cell(FP);
        %T{s,2} = num2cell(FP);
        T{s,3} = num2cell(FP);
    %T{s,4} = num2cell(FN);
    %T{s,5} = num2cell(FN);
    T{s,6} = num2cell(FN);   
        %T{s,7} = num2cell(TP);
        %T{s,8} = num2cell(TP);
        T{s,9} = num2cell(TP);
    %T{s,10} = num2cell(TN);
    %T{s,11} = num2cell(TN);
    T{s,12} = num2cell(TN);   
        disp(TP+FP+TN+FN)
end
%%
Tm = cell2mat(table2array(T));

FPR = zeros(12,3);
FPR(:,1) = Tm(:,1)./(Tm(:,1)+Tm(:,10));  % For S1
FPR(:,2) = Tm(:,2)./(Tm(:,2)+Tm(:,11));  % For S2
FPR(:,3) = Tm(:,3)./(Tm(:,3)+Tm(:,12));  % For S4
FPR = 100*FPR;
S2 = FPR(:,2); S2(S2==0) = 1; FPR(:,2) = S2;
S4 = FPR(:,3); S4(S4==0) = 1; FPR(:,3) = S4;
FPRf = [FPR([1 2],:); zeros(1,3); FPR([3 4],:);...
       zeros(2,3);...
       FPR([5 6],:); zeros(1,3); FPR([7 8],:);...
       zeros(2,3);...
       FPR([9 10],:); zeros(1,3); FPR([11 12],:)];

FNR = zeros(12,3);
FNR(:,1) = Tm(:,4)./(Tm(:,4)+Tm(:,7));  % For S1
FNR(:,2) = Tm(:,5)./(Tm(:,5)+Tm(:,8));  % For S2
FNR(:,3) = Tm(:,6)./(Tm(:,6)+Tm(:,9));  % For S4
FNR = 100*FNR;
S2 = FNR(:,2); S2(S2==0) = 1; FNR(:,2) = S2;
S4 = FNR(:,3); S4(S4==0) = 1; FNR(:,3) = S4;
FNRf = [FNR([1 2],:); zeros(1,3); FNR([3 4],:);...
       zeros(2,3);...
       FNR([5 6],:); zeros(1,3); FNR([7 8],:);...
       zeros(2,3);...
       FNR([9 10],:); zeros(1,3); FNR([11 12],:)];
   
%%
figure()
fsz = 10;
xlb = {'1S','2S','4S'};
yl = 60;
ylfnr = 45;

subplot(2,3,1), 
   v = [FNR(1,1) FNR(2,1);...
        FNR(1,2) FNR(2,2);...
        FNR(1,3) FNR(2,3)];
B1 = bar(v); ylim([0 ylfnr]) 
set(B1,'LineStyle','none');
titlelb = '\textbf{(a)} \begin{tabular}{c} VGG-19 \\ Layer A \end{tabular}';
title(titlelb,'interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
ylabel('FNR','interpreter','latex')

subplot(2,3,4),
   v = [FPR(1,1) FPR(2,1);...
        FPR(1,2) FPR(2,2);...
        FPR(1,3) FPR(2,3)];
B2 = bar(v); ylim([0 yl])
set(B2,'LineStyle','none');
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
ylabel('FPR','interpreter','latex')


subplot(2,3,2), 
   v = [FNR(5,1) FNR(6,1);...
        FNR(5,2) FNR(6,2);...
        FNR(5,3) FNR(6,3)];
B1 = bar(v); ylim([0 ylfnr]) 
set(B1,'LineStyle','none');
titlelb = '\textbf{(b)} \begin{tabular}{c} ResNet-101 \\ Layer A \end{tabular}';
title(titlelb,'interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
ylabel('FNR','interpreter','latex')

subplot(2,3,5), 
   v = [FPR(5,1) FPR(6,1);...
        FPR(5,2) FPR(6,2);...
        FPR(5,3) FPR(6,3)];
B2 = bar(v); ylim([0 yl]) 
set(B2,'LineStyle','none');
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
ylabel('FPR','interpreter','latex')


subplot(2,3,3), 
   v = [FNR(9,1) FNR(10,1);...
        FNR(9,2) FNR(10,2);...
        FNR(9,3) FNR(10,3)];
B1 = bar(v); ylim([0 ylfnr]) 
set(B1,'LineStyle','none');
titlelb = '\textbf{(c)} \begin{tabular}{c} Inception-ResNet-v2 \\ Layer A \end{tabular}';
title(titlelb,'interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
ylabel('FNR','interpreter','latex')

subplot(2,3,6), 
   v = [FPR(9,1) FPR(10,1);...
        FPR(9,2) FPR(10,2);...
        FPR(9,3) FPR(10,3)];
B2 = bar(v); ylim([0 yl]) 
set(B2,'LineStyle','none');
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
ylabel('FPR','interpreter','latex')

legend('Standard','Magnified','interpreter','latex','fontsize',fsz)


%%
figure()

subplot(2,1,1)
B = bar(FPRf,1.5); set(B,'LineStyle','none');
ylim([0 75]), grid on
set(gca,'ytick',0:10:70); ylabel('FPR','interpreter','latex')
labels = {'','','','','','','',...
             '','','','','','','',...
             '','','','',''};
set(gca,'xtick',1:19,'XTickLabel',labels);
set(gca,'TickLabelInterpreter','latex','fontsize',12);
set(gca,'XColor','k')
legend(B,{'1 Slice','2 Slices','4 Slices'},'interpreter','latex','fontsize',8)

subplot(2,1,2)
B = bar(FNRf,1.5); set(B,'LineStyle','none');
ylim([0 50]), grid on
set(gca,'ytick',0:10:40); ylabel('FNR','interpreter','latex')
labels = {'Std','Mag','','Std','Mag','','',...
             'Std','Mag','','Std','Mag','','',...
             'Std','Mag','','Std','Mag'};
set(gca,'xtick',1:19,'XTickLabel',labels);
set(gca,'TickLabelInterpreter','latex','fontsize',12);
set(gca,'XColor','k')
legend(B,{'1 Slice','2 Slices','4 Slices'},'interpreter','latex','fontsize',8)

pos = get(gca,'position')
set(gca,'position',[pos(1) pos(2)+0.1175 pos(3) pos(4)])
pos = [0.13,0.082,0.775,0.075];
str0 = '\,\,Layer A\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,Layer B';
str = [str0,'\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,',str0];
str = [str,'\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,',str0];
annotation('textbox',pos,'String',str,'FontSize',12,'Interpreter','latex','LineStyle','none')

str0 = 'VVG-19'; str1 = 'ResNet-101'; str2 = 'Inception-ResNet-v2';
str = ['\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,',...
    str0,'\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,',...
    str1,'\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,\,',...
    str2];
pos = [0.13,0.01,0.775,0.075];
annotation('textbox',pos,'String',str,'FontSize',12,'Interpreter','latex','LineStyle','none')

%%
%savePDFfig('acc')

figure()
fsz = 11;
fsz2 = 7;
ypos = 3;
s1p1 = 0.65; s1p2 = 1.05;
s2p1 = s1p1+1; s2p2 = s1p2+1;
s4p1 = s1p1+2; s4p2 = s1p2+2;
xlb = {'1S','2S','4S'};

subplot(2,3,1), 
v = [66.7 75; 70.8 83.3; 100 100];
B = bar(v); % VGG-19 Layer A: S1 S2 S4
%B(1).FaceColor = [0.3,0.75,0.93]; 
%B(2).FaceColor = [0.9,0.6,0.4]; 
B(1).LineStyle = 'none'; B(2).LineStyle = 'none';
B(1).FaceAlpha = 0.775; B(2).FaceAlpha = 0.775;
ylim([40 108])
titlelb = '\textbf{(a)} \begin{tabular}{c} VGG-19 \\ Layer A \end{tabular}';
title(titlelb,'interpreter','latex')
ylabel('Acc. (\%)','interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
text(s1p1,v(1,1)+ypos,num2str(v(1,1)),'interpreter','latex','fontsize',fsz2)
text(s1p2,v(1,2)+ypos,num2str(v(1,2)),'interpreter','latex','fontsize',fsz2)
text(s2p1,v(2,1)+ypos,num2str(v(2,1)),'interpreter','latex','fontsize',fsz2)
text(s2p2-0.05,v(2,2)+ypos,num2str(v(2,2)),'interpreter','latex','fontsize',fsz2)
text(s4p1,v(3,1)+ypos,num2str(v(3,1)),'interpreter','latex','fontsize',fsz2)
text(s4p2,v(3,2)+ypos,num2str(v(3,2)),'interpreter','latex','fontsize',fsz2)

subplot(2,3,4), 
v = [45.8 50; 100 100; 91.7 100];
B = bar(v); % VGG-19 Layer B: S1 S2 S4
B(1).LineStyle = 'none'; B(2).LineStyle = 'none';
B(1).FaceAlpha = 0.775; B(2).FaceAlpha = 0.775;
ylim([40 108])
titlelb = '\textbf{(b)} \begin{tabular}{c} VGG-19 \\ Layer B \end{tabular}';
title(titlelb,'interpreter','latex')
ylabel('Acc. (\%)','interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
text(s1p1,v(1,1)+ypos,num2str(v(1,1)),'interpreter','latex','fontsize',fsz2)
text(s1p2,v(1,2)+ypos,num2str(v(1,2)),'interpreter','latex','fontsize',fsz2)
text(s2p1,v(2,1)+ypos,num2str(v(2,1)),'interpreter','latex','fontsize',fsz2)
text(s2p2,v(2,2)+ypos,num2str(v(2,2)),'interpreter','latex','fontsize',fsz2)
text(s4p1,v(3,1)+ypos,num2str(v(3,1)),'interpreter','latex','fontsize',fsz2)
text(s4p2,v(3,2)+ypos,num2str(v(3,2)),'interpreter','latex','fontsize',fsz2)


subplot(2,3,2),
v = [75 54.2; 79.2 91.7; 87.5 100];
B = bar(v); % ResNet-101 Layer A: S1 S2 S4
B(1).LineStyle = 'none'; B(2).LineStyle = 'none';
B(1).FaceAlpha = 0.775; B(2).FaceAlpha = 0.775;
ylim([40 108])
titlelb = '\textbf{(c)} \begin{tabular}{c} ResNet-101 \\ Layer A \end{tabular}';
title(titlelb,'interpreter','latex')
ylabel('Acc. (\%)','interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
text(s1p1+0.1,v(1,1)+ypos,num2str(v(1,1)),'interpreter','latex','fontsize',fsz2)
text(s1p2,v(1,2)+ypos,num2str(v(1,2)),'interpreter','latex','fontsize',fsz2)
text(s2p1,v(2,1)+ypos,num2str(v(2,1)),'interpreter','latex','fontsize',fsz2)
text(s2p2-0.05,v(2,2)+ypos,num2str(v(2,2)),'interpreter','latex','fontsize',fsz2)
text(s4p1,v(3,1)+ypos,num2str(v(3,1)),'interpreter','latex','fontsize',fsz2)
text(s4p2,v(3,2)+ypos,num2str(v(3,2)),'interpreter','latex','fontsize',fsz2)

subplot(2,3,5),
v = [45.8 79.2; 91.7 95.8; 91.7 100];
B = bar(v); % ResNet-101 Layer B: S1 S2 S4
B(1).LineStyle = 'none'; B(2).LineStyle = 'none';
B(1).FaceAlpha = 0.775; B(2).FaceAlpha = 0.775;
ylim([40 108])
titlelb = '\textbf{(d)} \begin{tabular}{c} ResNet-101 \\ Layer B \end{tabular}';
title(titlelb,'interpreter','latex')
ylabel('Acc. (\%)','interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
text(s1p1,v(1,1)+ypos,num2str(v(1,1)),'interpreter','latex','fontsize',fsz2)
text(s1p2,v(1,2)+ypos,num2str(v(1,2)),'interpreter','latex','fontsize',fsz2)
text(s2p1,v(2,1)+ypos,num2str(v(2,1)),'interpreter','latex','fontsize',fsz2)
text(s2p2,v(2,2)+ypos,num2str(v(2,2)),'interpreter','latex','fontsize',fsz2)
text(s4p1,v(3,1)+ypos,num2str(v(3,1)),'interpreter','latex','fontsize',fsz2)
text(s4p2,v(3,2)+ypos,num2str(v(3,2)),'interpreter','latex','fontsize',fsz2)


subplot(2,3,3),
v = [58.3 66.7; 100 100; 100 100];
B = bar(v); % Inception-ResNet-v2 Layer A: S1 S2 S4
B(1).LineStyle = 'none'; B(2).LineStyle = 'none';
B(1).FaceAlpha = 0.775; B(2).FaceAlpha = 0.775;
ylim([40 108])
titlelb = '\textbf{(e)} \begin{tabular}{c} Inception-ResNet-v2 \\ Layer A \end{tabular}';
title(titlelb,'interpreter','latex')
ylabel('Acc. (\%)','interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
text(s1p1,v(1,1)+ypos,num2str(v(1,1)),'interpreter','latex','fontsize',fsz2)
text(s1p2,v(1,2)+ypos,num2str(v(1,2)),'interpreter','latex','fontsize',fsz2)
text(s2p1,v(2,1)+ypos,num2str(v(2,1)),'interpreter','latex','fontsize',fsz2)
text(s2p2,v(2,2)+ypos,num2str(v(2,2)),'interpreter','latex','fontsize',fsz2)
text(s4p1,v(3,1)+ypos,num2str(v(3,1)),'interpreter','latex','fontsize',fsz2)
text(s4p2,v(3,2)+ypos,num2str(v(3,2)),'interpreter','latex','fontsize',fsz2)

subplot(2,3,6), 
v = [54.2 62.5; 87.5 91.7; 91.7 83.3];
B = bar(v); % Inception-ResNet-v2 Layer B: S1 S2 S4
B(1).LineStyle = 'none'; B(2).LineStyle = 'none';
B(1).FaceAlpha = 0.775; B(2).FaceAlpha = 0.775;
ylim([40 108])
titlelb = '\textbf{(f)} \begin{tabular}{c} Inception-ResNet-v2 \\ Layer B \end{tabular}';
title(titlelb,'interpreter','latex')
ylabel('Acc. (\%)','interpreter','latex')
set(gca,'XTickLabel',xlb,'TickLabelInterpreter','latex')
set(gca,'fontsize',fsz,'fontweight','bold')
text(s1p1,v(1,1)+ypos,num2str(v(1,1)),'interpreter','latex','fontsize',fsz2)
text(s1p2,v(1,2)+ypos,num2str(v(1,2)),'interpreter','latex','fontsize',fsz2)
text(s2p1,v(2,1)+ypos,num2str(v(2,1)),'interpreter','latex','fontsize',fsz2)
text(s2p2,v(2,2)+ypos,num2str(v(2,2)),'interpreter','latex','fontsize',fsz2)
text(s4p1,v(3,1)+ypos,num2str(v(3,1)),'interpreter','latex','fontsize',fsz2)
text(s4p2,v(3,2)+ypos,num2str(v(3,2)),'interpreter','latex','fontsize',fsz2)

legend('Standard','Magnified','interpreter','latex','fontsize',10)


%%
