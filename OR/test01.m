clear, clc, close all
load tables.mat RL_mag RL_std

tab_names = {'RL_std','RL_mag'};

for m = 1:length(tab_names)
    fprintf('Results for %s\n', tab_names{m})
    eval(sprintf('tab = %s;',tab_names{m}));
    labels = strcmp(tab.patient_class,'parkinson');
    nets = {'net1','net2','net3','net4','net5','net6'};
    results = array2table(zeros(3,6), 'VariableNames', nets, 'RowNames', {'OR','ORx','pValue'});
    for n = 1:length(nets)
        scores = tab.(nets{n});
        adj_vars = [tab.age, strcmp(tab.sex,'M')];
        [OR, ORx,~,~,pval] = adjusted_OR(labels, scores, adj_vars);
        results.(nets{n})(1) = OR;
        results.(nets{n})(2) = ORx;
        results.(nets{n})(3) = pval;
    end
    disp(results)
end

%Compare AUCs with DeLong's test
results = array2table(zeros(3,6), 'VariableNames', nets, 'RowNames',{'AUC_std', 'AUC_mag','pValue'});
for n = 1:length(nets)
    y = strcmp(RL_std.patient_class, 'parkinson');
    res = AUC_compare_correlated([RL_std.(nets{n}), RL_mag.(nets{n})], y);
    results.(nets{n})(1) = res{1}(1);
    results.(nets{n})(2) = res{2}(1);
    results.(nets{n})(3) = res{3}(3);
end
fprintf('AUC comparison (using DeLongs test)\n')
disp(results)