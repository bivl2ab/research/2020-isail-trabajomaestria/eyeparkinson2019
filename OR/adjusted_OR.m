function [OR, ORx, OR_CI, ORx_CI] = adjusted_OR(labels, scores, adj_vars)
% Adjust OR for control variables
% Sintax:
%     [OR, ORx] = adjusted_OR(scores, labels, adj_vars)
% Inputs:
%     labels,     Nx1 vector with binary labels (1=case, 0 = control)
%     scores,     Nx1 vector with predicted scores
%     adj_vars,   NxM vector with M adjusting variables
% Outputs:
%     OR,         Odds ratios without adjusting
%     ORx,        Adjusted odds ratios
%     px,         pValue of adjusted OR
%     
% S.Pertuz
% 01.01.2019

x = zscore(labels(:));
y = scores(:);

%Compute OR's (without adjusting) with CI's
[B, ~, stats] = glmfit(x, y, 'binomial', 'link', 'logit');
B_lo = B(2)-1.965*stats.se(2);  %Lower bound
B_hi = B(2)+1.965*stats.se(2);  %Upper bound
OR = exp(B(2));
OR_CI(1) = exp(B_lo); %CI lower limit
OR_CI(2) = exp(B_hi); %CI higher limit   
OR_CI(3) = stats.p(2); %pvalue

%Compute OR's (with adjusting) with CI's
x = [x, zscore(adj_vars)];
[B, ~, stats] = glmfit(x, y, 'binomial', 'link', 'logit');
B_lo = B(2)-1.965*stats.se(2);  %Lower bound
B_hi = B(2)+1.965*stats.se(2);  %Upper bound
ORx = exp(B(2));
ORx_CI(1) = exp(B_lo); %CI lower limit
ORx_CI(2) = exp(B_hi); %CI higher limit   
ORx_CI(3) = stats.p(2); %pvalue