%{
I = imread('r.png');
for i = 1:100
    F(i).data = I;
end
I = imread('g.png');
for i = 101:200
    F(i).data = I;
end
I = imread('b.png');
for i = 201:300
    F(i).data = I;
end

St(:,:,1) = S(:,:,1)'; 
St(:,:,2) = S(:,:,2)';
St(:,:,3) = S(:,:,3)';
imwrite(St,strcat(listing(l).name(1:end-4),'_S1t.png'))
Sf(:,:,1) = flip(S(:,:,1)); 
Sf(:,:,2) = flip(S(:,:,2));
Sf(:,:,3) = flip(S(:,:,3));
imwrite(Sf,strcat(listing(l).name(1:end-4),'_S1f.png'))
%}

%% Slicing
clear all, clc
%cd data3\DER\
cd data3\IZQ\
%cd results3\DER\
%cd results3\IZQ\
%cd results3\zfreqs
listing = dir('*.avi');
tic
for l = 1:1 %length(listing)
    disp(l)
    slices = cell(8,1);
    % S1: XT-Slice
    pts.yP = 70;
    slices{1} = getSlice(listing(l).name,'horizontal',pts);
    % S2: 45�-Slice
    pts.P1 = [1 140]; pts.P2 = [210 1];
    slices{2} = getSlice(listing(l).name,'diagonal',pts);
    % S3: YT-Slice
    pts.xP = 105;
    slices{3} = getSlice(listing(l).name,'vertical',pts);
    % S4: 135�-Slice
    pts.P1 = [1 1]; pts.P2 = [210 140]; 
    slices{4} = getSlice(listing(l).name,'diagonal',pts);
    % S5:
    pts.P1 = [1 105]; pts.P2 = [210 35];
    slices{5} = getSlice(listing(l).name,'diagonal',pts);
    % S6:
    pts.P1 = [53 140]; pts.P2 = [158 1];
    slices{6} = getSlice(listing(l).name,'diagonal',pts);
    % S7:
    pts.P1 = [53 1]; pts.P2 = [158 140];
    slices{7} = getSlice(listing(l).name,'diagonal',pts);
    % S8:
    pts.P1 = [1 35]; pts.P2 = [210 105];
    slices{8} = getSlice(listing(l).name,'diagonal',pts);

    % Saving 
    %save([listing(l).name(1:end-4),'_newSlcs.mat'],'slices') 
end
toc
cd ..\..

%% Contrast Enhancement
clear all, clc
%cd data3\DER\
%cd data3\IZQ\
%cd results3\DER\
cd results3\IZQ\
%cd results3\zfreqs
listing = dir('*_newSlcs.mat');
for l = 1:length(listing)
    disp(l)
    load(listing(l).name)
    slices_e = cell(8,1);
    for s = 1:8
        slc_inv = imcomplement(slices{s});
        slices_e{s} = imcomplement(imreducehaze(slc_inv));
        %imwrite(slc1_e,[listing(l).name(1:end-4),'_slc1_e.png'])  
    end
    save(listing(l).name,'slices_e')  
end
cd ..\..

%{
load(listing(l).name)

slc1_lab = rgb2lab(slc1);
L = slc1_lab(:,:,1)/100;
slc1_lab_e1 = slc1_lab;
slc1_lab_e2 = slc1_lab;
slc1_lab_e3 = slc1_lab;

slc1_lab_e1(:,:,1) = imadjust(L)*100;
slc1_e1 = lab2rgb(slc1_lab_e1);
slc1_lab_e2(:,:,1) = histeq(L)*100;
slc1_e2 = lab2rgb(slc1_lab_e2);
slc1_lab_e3(:,:,1) = adapthisteq(L)*100;
slc1_e3 = lab2rgb(slc1_lab_e3);

slc1_inv = imcomplement(slc1);
slc1_inv = imreducehaze(slc1_inv);
slc1_e4 = imcomplement(slc1_inv);

montage({slc1,slc1_e1,slc1_e2,slc1_e3,slc1_e4},'Size',[1 5])
%}
%% Fixing slc1_e transpose & slc3_e size : 140x300 => 210x300
clear all, clc
%cd data3\DER\
%cd data3\IZQ\
%cd results3\DER\
cd results3\IZQ\
%cd results3\zfreqs
listing = dir('*_newSlcs.mat');
for l = 1:length(listing)
    disp(l)
    load(listing(l).name)
    
    Tmp = zeros(size(slices_e{2}),'uint8'); 
    for i = 1:35
        Tmp(i,:,:) = slices_e{3}(1,:,:);
    end
    Tmp(36:175,:,:) = slices_e{3}(1:140,:,:);
    for i = 176:210
        Tmp(i,:,:) = slices_e{3}(140,:,:);
    end
    slices_e{3} = Tmp;
    Tmp(:,:,1) = slices_e{1}(:,:,1)';
    Tmp(:,:,2) = slices_e{1}(:,:,2)';
    Tmp(:,:,3) = slices_e{1}(:,:,3)';
    slices_e{1} = Tmp;
    %imwrite(slc1_e,[listing(l).name(1:end-4),'_slc1_e.png'])  
    %imwrite(slc3_e,[listing(l).name(1:end-4),'_slc3_e.png'])      
    save(listing(l).name,'slices_e')     
end
cd ..\..

%% Fixing results3 slices: 299x210 = 300x210
clear all, clc
%cd results3\DER\
cd results3\IZQ\
%cd results3\zfreqs
listing = dir('*_newSlcs.mat');
for l = 1:length(listing)
    disp(l)
    load(listing(l).name)
    for s = 1:8
        slices_e{s}(:,300,:) = slices_e{s}(:,299,:);
        %imwrite(slc1_e,[listing(l).name(1:end-4),'_slc1_e.png'])       
    end
    save(listing(l).name,'slices_e')         
end
cd ..\..
%% End
cd Temp.Figs\
listing = dir('*.avi');
cd ..
slices = cell(7,1);
for l = 1:length(listing)
    % S3: YT-Slice
    pts.xP = 105;
    slices{l} = getSlice(listing(l).name,'vertical',pts);
end

for i = 1:5 
   slc = slices{i+1}(:,1:210,:); % 120:299
   imwrite(slc,[num2str(i),'.png'])
end

alph = [5 10 15 20 25];
%mask = zeros(140,210); mask(:,16:195) = 1;
%slc_a = zeros(140,210,3,'uint8');
figure
ha = tight_subplot(1,7,.01,.01,.01);
axes(ha(2)), imshow(imread('PK03_DER_S3.png')), xline(105,'g','LineWidth',2.5);
for i = 1:5 
   axes(ha(i+2))
   %slc = slices{i+1}(:,90:299,:); % 120:299
   %slc_a(:,16:195,:) = slc;
   %image(slc_a,'AlphaData',mask); axis image, set(gca,'visible','off')
   imshow(imread([num2str(i),'.png']))
   lbl = ['\boldmath$\alpha = ',num2str(alph(i)),'$'];
   if i==1, xt = 60; else, xt = 54; end
   text(xt,167.5,lbl,'FontSize',17,'interpreter','latex')
end

savePDFfig('eyeMags')


