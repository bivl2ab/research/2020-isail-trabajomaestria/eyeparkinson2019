%% Filenames
clear all, clc
%addpath('data3\DER\'), cd data3\DER\
%addpath('data3\IZQ\') ,cd data3\IZQ\
%addpath('results3\DER\') ,cd results3\DER\
addpath('results3\IZQ\') ,cd results3\IZQ\
listing = dir('*.mat'); cd ..\..
%% Data Augmentation
concatslices = cell(length(listing),4,4); % (#subjects,#colorChannels,#flips)
for l = 1:length(listing)
    disp(l)
    load(listing(l).name)  
    augslices = cell(4,4,4); % (#slices,#colorChannels,#flips)
    for s = 1:4
       % Red Channel
       augslices{s,1,1} = slices_e{s}(:,:,1); % f0
       augslices{s,1,2} = flip(slices_e{s}(:,:,1),2); % fh
       augslices{s,1,3} = flip(slices_e{s}(:,:,1),1); % fv
       augslices{s,1,4} = flip(augslices{s,1,2},1); % fhv
       % Green Channel
       augslices{s,2,1} = slices_e{s}(:,:,2); % f0
       augslices{s,2,2} = flip(slices_e{s}(:,:,2),2); % fh
       augslices{s,2,3} = flip(slices_e{s}(:,:,2),1); % fv
       augslices{s,2,4} = flip(augslices{s,2,2},1); % fhv
       % Blue Channel
       augslices{s,3,1} = slices_e{s}(:,:,3); % f0
       augslices{s,3,2} = flip(slices_e{s}(:,:,3),2); % fh
       augslices{s,3,3} = flip(slices_e{s}(:,:,3),1); % fv
       augslices{s,3,4} = flip(augslices{s,3,2},1); % fhv
       % GrayScale
       augslices{s,4,1} = rgb2gray(slices_e{s}); % f0
       augslices{s,4,2} = flip(rgb2gray(slices_e{s}),2); % fh
       augslices{s,4,3} = flip(rgb2gray(slices_e{s}),1); % fv
       augslices{s,4,4} = flip(augslices{s,4,2},1); % fhv   
    end
    for cch = 1:4
        for f = 1:4
            concatslices{l,cch,f}(:,:,1) = augslices{1,cch,f};
            concatslices{l,cch,f}(:,:,2) = augslices{2,cch,f};
            concatslices{l,cch,f}(:,:,3) = augslices{3,cch,f};
            concatslices{l,cch,f}(:,:,4) = augslices{4,cch,f};            
        end
    end  
end
%save('data3_DER_concatslcs.mat','concatslices') 
%save('data3_IZQ_concatslcs.mat','concatslices') 
%save('results3_DER_concatslcs.mat','concatslices') 
save('results3_IZQ_concatslcs.mat','concatslices') 

%% Kfold Trainings
clear all, clc
load myCNN3_4slc.mat

drloads = {'data3_DER_concatslcs.mat','data3_IZQ_concatslcs.mat',...
        'results3_DER_concatslcs.mat','results3_IZQ_concatslcs.mat'};

options = trainingOptions('adam', ...
            'InitialLearnRate',0.01, ...
            'MaxEpochs',8, ...
            'Shuffle','every-epoch', ...
            'Verbose',false, ...
            'Plots','none',... %'training-progress','none'    
            'ExecutionEnvironment','gpu');
%% RUN!
drid = 2; %<##############
for drid = 1:4 
    disp(['==',drloads{drid},'======================'])
    load(drloads{drid})
    pslices = cell(26,5,4,4);
    for p = 1:26
        for smpl = 1:5
            for cch = 1:4
                for f = 1:4
                    pslices(p,smpl,cch,f) = concatslices(smpl+(p-1)*5,cch,f);
                end
            end
        end
    end
    clear concatslices p smpl cch f

    idx = cell(26,1); c = 1;
    for p = 1:26
        for s = 1:80 % 5*4*4
            idx{p}(s) = c;
            c = c+1;
        end
    end
    labels = [zeros(26*80/2,1);ones(26*80/2,1)]; clear p s c

    acc = zeros(26,1);
    kTrainedNets = cell(26,1); YPred = cell(26,1); scores = cell(26,1);
    k = 4; %<##############
    for k = 1:26
        disp(['Fold ',num2str(k)])
        cellTest = squeeze(pslices(k,:,1:3,1)); 
        cellTest = cellTest(:);
        imdsTest = cat(4,cellTest{:});
        labelsTest = labels(idx{k}(1:15)); 
        cellTrain = reshape(pslices(setdiff(1:26,k),:,:,:),25,[]); 
        cellTrain = cellTrain'; cellTrain = cellTrain(:);
        imdsTrain = cat(4,cellTrain{:});
        labelsTrain = labels(setdiff(1:2080,idx{k}));    
        for i = 1:10 %<##############
            disp(i) % <########
            %rng default
            kTrainedNets{i} = trainNetwork(imdsTrain,categorical(labelsTrain),layers,options);   
            [YPred{i},scores{i}] = classify(kTrainedNets{i},imdsTest);
            accTune(i) = sum(YPred{i} == categorical(labelsTest))/numel(labelsTest); %<### acc(k)
            disp(['Acc: ',num2str(accTune(i))])
            pause(3) %<##############
        end %<##############
        
        clear imdsTrain cellTrain labelsTrain ...
              imdsTest cellTest labelsTest
        pause(3)
    end
    save([drloads{drid}(1:end-14),'CNNTrain.mat'],'kTrainedNets','acc','YPred','scores')
end

k6TrainedNet = kTrainedNets{3};
acck6 = accTune(3);
YPredk6 = YPred{3};
scoresk6 = scores{3};
clear kTrainedNets accTune YPred scores
save('r3_IZQ_k6.mat','k6TrainedNet','acck6','YPredk6','scoresk6')
clear k6TrainedNet acck6 YPredk6 scoresk6

%{
clear TMP
for i = 1:10
    for j = 1:4
        for k = 1:3
            for q = 1:2
                TMP{i,j,k,q} = [i j k q];
            end
        end
    end
end
TMP_rshp = reshape(TMP,10,[]);
TMP_rshp_t = TMP_rshp';
TMP_flt = TMP_rshp_t(:);
%}
  %  0.9333
  %  0.9231

  %  0.8897 => 0.9436
  %  0.8795 => 0.9282
%% Plotting
clear all, clc

load data3_DER_CNNTrain.mat acc scores
accDder = acc; scoresDder = scores;
load results3_DER_CNNTrain.mat acc scores
accRder = acc; scoresRder = scores;

load data3_IZQ_CNNTrain.mat acc scores
accDizq = acc; scoresDizq = scores;
load results3_IZQ_CNNTrain.mat acc scores
accRizq = acc; scoresRizq = scores;

load data3_STR_CNNTrain.mat acc scores
accDstr = acc; scoresDstr = scores;
load results3_STR_CNNTrain.mat acc scores
accRstr = acc; scoresRstr = scores;

clear acc scores
%==========================================================================
figure
subplot(1,2,1)
bar(diag([mean(accDder) mean(accRder)]),'stacked')
ylim([0.8 1]), grid on, title('Right Eye')
subplot(1,2,2)
bar(diag([mean(accDizq) mean(accRizq)]),'stacked')
ylim([0.8 1]), grid on, title('Left Eye')
legend('Standard','Magnified','fontsize',10)
%==========================================================================
gtlabels = [zeros(26*15/2,1);ones(26*15/2,1)];
figure
subplot(1,2,1)
catDscores = cat(1,scoresDder{:});
[X,Y,T,AUCD] = perfcurve(gtlabels,catDscores(:,2),1);
plot(X,Y,'LineWidth',1,'Color',[0.4660 0.6740 0.1880]), hold on
catRscores = cat(1,scoresRder{:});
[X,Y,T,AUCR] = perfcurve(gtlabels,catRscores(:,2),1);
plot(X,Y,'LineWidth',1,'Color',[0 1 0]), grid on, title('Right Eye')
comp = AUC_compare_correlated([catDscores(:,2),catRscores(:,2)], gtlabels);
legend(sprintf('  std AUC %%95CI: %0.3f [%0.3f - %0.3f]',comp{1}(1),comp{1}(2),comp{1}(3)),...
       sprintf('mag AUC %%95CI: %0.3f [%0.3f - %0.3f]',comp{2}(1),comp{2}(2),comp{2}(3)));
text(0.5,0.5,sprintf('p-value: %0.3f',comp{4}))



subplot(1,2,2)
catDscores = cat(1,scoresDizq{:});
[X,Y,T,AUCD] = perfcurve(gtlabels,catDscores(:,2),1);
plot(X,Y,'LineWidth',1,'Color',[0.8500 0.3250 0.0980]), hold on
catRscores = cat(1,scoresRizq{:});
[X,Y,T,AUCR] = perfcurve(gtlabels,catRscores(:,2),1);
plot(X,Y,'LineWidth',1,'Color',[1 0 0]), grid on, title('Left Eye')
comp = AUC_compare_correlated([catDscores(:,2),catRscores(:,2)], gtlabels);
legend(sprintf('  std ROC %%95CI: %0.3f [%0.3f - %0.3f]',comp{1}(1),comp{1}(2),comp{1}(3)),...
       sprintf('mag ROC %%95CI: %0.3f [%0.3f - %0.3f]',comp{2}(1),comp{2}(2),comp{2}(3)));
text(0.5,0.5,sprintf('p-value: %0.3f',comp{4}))

savePDFfig('roc_CNN')

